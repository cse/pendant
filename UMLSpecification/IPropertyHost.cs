﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    /// <summary>
    /// An interface implemented by definitions that host properties
    /// </summary>
    public interface IPropertyHost
    {
        /// <summary>
        /// The name of the host
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The kind of host (i.e. interface, class)
        /// </summary>
        public string Kind { get; }

        /// <summary>
        /// A list of properties possessed by the host
        /// </summary>
        public List<PropertyDefinition> Properties { get; }
    }
}
