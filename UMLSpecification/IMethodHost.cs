﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    /// <summary>
    /// An interface implemented by definitions that host methods
    /// </summary>
    public interface IMethodHost
    {
        /// <summary>
        /// The name of the host
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The kind of host (i.e. interface, class)
        /// </summary>
        public string Kind { get; }

        /// <summary>
        /// A list of methods possessed by the host
        /// </summary>
        public List<MethodDefinition> Methods { get; }
    }
}
