﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public class ParameterDefinition
    {
        public string Name { get; set; }

        public Visibility Visibility { get; set; }

        public string Type { get; set; }
    }
}
