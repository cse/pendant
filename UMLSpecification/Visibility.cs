﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public enum Visibility
    {
        Public,
        Package, 
        Protected,
        Private
    }
}
