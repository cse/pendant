﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public class MethodDefinition
    {
        public string Name { get; set; }

        public Visibility Visibility { get; set; }

        public bool Abstract { get; set; }

        public bool Virtual { get; set; }

        public bool Override { get; set; }

        public string ReturnType { get; set; }

        public List<ParameterDefinition> Parameters { get; } = new List<ParameterDefinition>();
    }
}
