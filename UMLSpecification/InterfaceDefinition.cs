﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public class InterfaceDefinition : IPropertyHost, IMethodHost
    {
        /// <summary>
        /// The interface name.  This must be specified
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// What this definition represents
        /// </summary>
        public string Kind => "interface";

        public Visibility Visibility { get; set; }

        public List<PropertyDefinition> Properties { get; } = new List<PropertyDefinition>();

        public List<MethodDefinition> Methods { get; } = new List<MethodDefinition>();

        public List<EventDefinition> Events { get; } = new List<EventDefinition>();

        public List<IndexerDefinition> Indexers { get; } = new List<IndexerDefinition>();
    }
}
