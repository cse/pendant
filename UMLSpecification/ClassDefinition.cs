﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public class ClassDefinition : IPropertyHost, IMethodHost
    {
        /// <summary>
        /// The interface name.  This must be specified
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// What this definition represents
        /// </summary>
        public string Kind => "class";

        /// <summary>
        /// If this class is declared abstract
        /// </summary>
        public bool Abstract { get; set; }

        /// <summary>
        /// The base class (if any) of this class
        /// </summary>
        public string BaseClass { get; set; }

        public Visibility Visibility { get; set; }

        public List<FieldDefinition> Fields { get; } = new List<FieldDefinition>();

        public List<PropertyDefinition> Properties { get; } = new List<PropertyDefinition>();

        public List<MethodDefinition> Methods { get; } = new List<MethodDefinition>();

        public List<EventDefinition> Events { get; } = new List<EventDefinition>();

        public List<IndexerDefinition> Indexers { get; } = new List<IndexerDefinition>();
    }
}
