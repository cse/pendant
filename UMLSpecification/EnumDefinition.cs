﻿using System;
using System.Collections.Generic;

namespace UMLSpecification
{
    public class EnumMemberDefinition
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }

    public class EnumDefinition
    {
        /// <summary>
        /// The enum name.  This must be specified
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The visibility of the enumeration
        /// </summary>
        public Visibility Visibility { get; set; }

        /// <summary>
        /// The underlying integral numeric type used for the enum members.
        /// If not specified defaults to an int
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The members of this enumeration
        /// </summary>
        public List<EnumMemberDefinition> Members { get; } = new List<EnumMemberDefinition>();
    }
}
