﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public class ProjectDefinition
    {
        public string Name { get; set; }

        public List<EnumDefinition> Enums { get; } = new List<EnumDefinition>();

        public List<InterfaceDefinition> Interfaces { get; } = new List<InterfaceDefinition>();

        public List<ClassDefinition> Classes { get; } = new List<ClassDefinition>();

    }
}
