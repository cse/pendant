﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public class SolutionDefinition
    {
        public string Name { get; set; }

        public List<ProjectDefinition> Projects { get; } = new List<ProjectDefinition>();
    }
}
