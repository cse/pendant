﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLSpecification
{
    public class PropertyDefinition
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public Visibility Visibility { get; set; }

        public bool Abstract { get; set; }

        public bool Virtual { get; set; }

        public bool Override { get; set; }

        public bool Get { get; set; }

        public bool Set { get; set; }
    }
}
