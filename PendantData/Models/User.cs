﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    [Flags]
    public enum UserRoles : int
    {
        None = 0,
        Student = 1,
        // Staff = 16,
        Faculty = 32,
        Admin = 128
    }

    public class User
    {
        /// <summary>The database primary key</summary>
        public int ID { get; set; }

        /// <summary>The PortalUsers's EID</summary>
        public string EID { get; set; }

        /// <summary>The PortalUsers's last name</summary>
        public string LastName { get; set; }

        /// <summary>The PortalUsers's first name</summary>
        public string FirstName { get; set; }

        /// <summary>A bitmask indicating the user's roles</summary>
        public UserRoles Roles { get; set; }

        /// <summary>The PortalUser's email address</summary>
        public string Email => $"{EID}@ksu.edu";

        /// <summary>The PortalUser's full name</summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>Determines if this user is a student</summary>
        [NotMapped]
        public bool IsStudent
        {
            get => Roles.HasFlag(UserRoles.Student);
            set
            {
                if (value) Roles &= UserRoles.Student;
                else Roles &= ~UserRoles.Student;
            }
        }

        /// <summary>Determines if this user is faculty</summary>
        [NotMapped]
        public bool IsFaculty
        {
            get => Roles.HasFlag(UserRoles.Faculty);
            set
            {
                if (value) Roles &= UserRoles.Faculty;
                else Roles &= ~UserRoles.Faculty;
            }
        }

        /// <summary>Determines if this user is an admin</summary>
        [NotMapped]
        public bool IsAdmin
        {
            get => Roles.HasFlag(UserRoles.Admin);
            set
            {
                if (value) Roles &= UserRoles.Admin;
                else Roles &= ~UserRoles.Admin;
            }
        }

        /// <summary>
        /// The valiation attempts this user has made
        /// </summary>
        public ICollection<ValidationAttempt> ValidationAttempts { get; set; }

        /// <summary>
        /// All checks against diagnostic codes made by this user
        /// </summary>
        public ICollection<DiagnosticCodeLookup> DiagnosticCodeLookups { get; set; }

        /// <summary>
        /// The GitHub Account that belongs to this user
        /// </summary>
        public GitHubAccount GitHubAccount { get; set; }

        /// <summary>
        /// All franchise locations that belong to this user
        /// </summary>
        public ICollection<FranchiseLocation> FranchiseLocations { get; set; } = Array.Empty<FranchiseLocation>();
    }
}
