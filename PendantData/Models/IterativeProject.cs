﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class representing a iteratively-developed project composed of milestone assignments
    /// </summary>
    public class IterativeProject
    {
        /// <summary>
        /// The database primary key
        /// </summary>
        public int ID { get; set; }
                
        /// <summary>
        /// The course this assignment series belongs to
        /// </summary>
        public Course Course{ get; set; }

        /// <summary>
        /// The foreign key to the course
        /// </summary>
        [Required]
        public int CourseID { get; set; }

        /// <summary>
        /// The name of the semester project
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The repository prefix assigned by GitHub classroom to the student repositories
        /// </summary>
        [Required]
        public string RepositoryPrefix { get; set; }

        /// <summary>
        /// The GitHub organization assocaited with this assignment
        /// </summary>
        [Required]
        public string GitHubOrganizationName { get; set; } = "ksu-cis";

        /// <summary>
        /// The GitHub Classroom assignment invitation URL
        /// </summary>      
        [Required]
        public string AssignmentInvitationUrl { get; set; }

        /// <summary>
        /// Returns a string identifying this assignment series
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{Name} Iterative Project";

        /// <summary>
        /// The milestones that belong to this iterative project
        /// </summary>
        public ICollection<MilestoneAssignment> Milestones { get; set; }
    }
}
