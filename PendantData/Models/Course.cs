﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class representing a university course
    /// </summary>
    public class Course
    {
        /// <summary>
        /// The database primary key
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The name of the course
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The assignments associated with this course
        /// </summary>
        public ICollection<CourseAssignment> Assignments { get; set; }

        /// <summary>
        /// The iterative projects associated with this course
        /// </summary>
        public ICollection<IterativeProject> IterativeProjects { get; set; }

        /// <summary>
        /// Returns this course as a string
        /// </summary>
        /// <returns>The name of the course</returns>
        public override string ToString() => Name;
    }
}
