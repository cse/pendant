﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class representing a fast-food Franchise 
    /// </summary>
    /// <remarks>
    /// This class is used for the CIS 400 course
    /// </remarks>
    public class Franchise
    {
        /// <summary>
        /// The database primary key
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The name of this franchise
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
