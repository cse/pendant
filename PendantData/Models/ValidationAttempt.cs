﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class representing an attempt by a student 
    /// to validate thier assignment solution
    /// </summary>
    public class ValidationAttempt
    {
        /// <summary>The database primary key</summary>
        public int ID { get; set; }

        /// <summary>
        /// The user who made the attempt
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// The assignment this attempt is for
        /// </summary>
        public Assignment Assignment { get; set; }

        /// <summary>
        /// The date and time the attempt was made
        /// </summary>
        public DateTime DateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// The URL of the student's repo
        /// </summary>
        public string RepoUrl { get; set; }

        /// <summary>
        /// The identifier of the commit 
        /// </summary>
        public string CommitIdentifier { get; set; }

        /// <summary>
        /// The outcome of the validation attempt
        /// </summary>
        public string Report { get; set; } = "<div class=\"spinner-border\" role=\"status\"><span class=\"sr-only\">Loading...</span></div>";

        /// <summary>
        /// If the validation attempt has been checked
        /// </summary>
        public bool IsChecked { get; set; }

        /// <summary>
        /// The total number of issues encountered in the attempt
        /// </summary>
        public int IssueCount => StructuralIssues.Count + FunctionalIssues.Count + DesignIssues.Count + StyleIssues.Count;

        /// <summary>
        /// The structural issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> StructuralIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The functional issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> FunctionalIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The design issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> DesignIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The style issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> StyleIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The location of the student's work in the file system
        /// </summary
        public string SolutionPath { get; set; }
    }
}
