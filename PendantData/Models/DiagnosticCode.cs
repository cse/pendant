﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KSU.CS.Pendant.Server.Models
{
    public class DiagnosticCode
    {
        /// <summary>The database primary key</summary>
        public int ID { get; set; }

        /// <summary>The code identifying the diagnosed issue</summary>
        [Required]
        public string Code { get; set; }

        /// <summary>The name of the diagnosed issue</summary>
        [Required]
        public string Name { get; set; }

        /// <summary>A markdown string describing the diagnosed issue</summary>
        [Required]
        public string Markdown { get; set; }

        /// <summary>The HTML generated from the markdown string</summary>
        public string Html { get; set; }

        /// <summary>All checks made against this diagnostic code</summary>
        public ICollection<DiagnosticCodeLookup> DiagnosticCodeLookups { get; set; }
    }
}
