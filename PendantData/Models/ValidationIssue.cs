﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    [NotMapped]
    public class ValidationIssue
    {
        public string Message { get; set; }

        public string HelpUrl { get; set; }

        public string SourceUrl { get; set; }
    }
}
