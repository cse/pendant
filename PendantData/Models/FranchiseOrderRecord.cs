﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class representing an order taken at a franchise location
    /// </summary>
    public class FranchiseOrderRecord
    {
        /// <summary>
         /// The database primary key
         /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The franchise location this order is from
        /// </summary>
        [Required]
        public FranchiseLocation FranchiseLocation { get; set; }

        /// <summary>
        /// The number for this order
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// The subtotal for this order
        /// </summary>
        [Column(TypeName = "money")]
        public decimal Subtotal { get; set; }

        /// <summary>
        /// The tax for this order
        /// </summary>
        [Column(TypeName = "money")]
        public decimal Tax { get; set; }

        /// <summary>
        /// The Total for this order
        /// </summary>
        [Column(TypeName = "money")]
        public decimal Total { get; set; }

        /// <summary>
        /// The date and time of this order
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// The items in this order, as a JSON string
        /// </summary>
        public string Items { get; set; }
    }
}
