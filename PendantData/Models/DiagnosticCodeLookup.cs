﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Models
{
    public class DiagnosticCodeLookup
    {
        /// <summary>The database primary key</summary>
        public int ID { get; set; }

        /// <summary>The user who requested this check</summary>
        public User User { get; set; }

        /// <summary>The Diagnostic Code this check was for</summary>
        public DiagnosticCode DiagnosticCode { get; set; }

        /// <summary>The Date and Time the check was performed</summary>
        public DateTime DateTime { get; set; } = DateTime.Now;
    }
}
