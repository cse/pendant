﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class representing a Franchise location
    /// </summary>
    public class FranchiseLocation
    {
        /// <summary>
        /// The database primary key
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The franchise this location belongs to
        /// </summary>
        public Franchise Franchise { get; set; }

        /// <summary>
        /// The user this location belongs to
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// The name of this location
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The latitude of this location 
        /// </summary>
        [Required]
        public double Latitude { get; set; }

        /// <summary>
        /// The lognitude of this location 
        /// </summary>
        [Required]
        public double Longitude { get; set; }

        /// <summary>
        /// The records of orders made at this location 
        /// </summary>
        public ICollection<FranchiseOrderRecord> OrderRecords { get; set; }

        /// <summary>
        /// A secret used to validate webhook requests for this location
        /// </summary
        public string Secret { get; private set; }

        /// <summary>
        /// Generates a new secret for this Franchise Location
        /// A location will have only one secret at a time.
        /// </summary>
        public void GenerateSecret()
        {
            Secret = Guid.NewGuid().ToString();
        }
    }
}
