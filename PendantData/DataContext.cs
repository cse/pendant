﻿using System.Collections.Generic;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using KSU.CS.Pendant.Server.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;

namespace KSU.CS.Pendant.Server
{
    public class DataContext : DbContext
    {
        /// <summary>
        /// Constructs a new DataContext
        /// </summary>
        /// <param name="options">Options to configure the DbContext</param>
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<KSU.CS.Pendant.Server.Models.User> Users { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.Course> Courses { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.Assignment> Assignments { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.CourseAssignment> CourseAssignments { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.IterativeProject> IterativeProjects { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.MilestoneAssignment> MilestoneAssignments { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.ValidationAttempt> ValidationAttempts { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.DiagnosticCode> DiagnosticCodes { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.DiagnosticCodeLookup> DiagnosticCodeLookups { get; set; }

        public DbSet<KSU.CS.Pendant.Server.Models.GitHubAccount> GitHubAccounts { get; set; }

        public DbSet<Franchise> Franchises { get; set; }

        public DbSet<FranchiseLocation> FranchiseLocations { get; set; }

        public DbSet<FranchiseOrderRecord> FranchiseOrderRecords { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Core tables
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<User>().Property(u => u.Roles).HasConversion<int>();
            modelBuilder.Entity<User>().HasData(
                new User() { ID = 1, EID = "nhbean", FirstName = "Nathan", LastName = "Bean", Roles = UserRoles.Admin | UserRoles.Faculty },
                new User() { ID = 2, EID = "russfeld", FirstName = "Russ", LastName = "Feldhausen", Roles = UserRoles.Admin | UserRoles.Faculty },
                new User() { ID = 3, EID = "weeser", FirstName = "Josh", LastName = "Weese", Roles = UserRoles.Admin | UserRoles.Faculty }
            );

            modelBuilder.Entity<Course>().ToTable("Course");
            modelBuilder.Entity<Course>().HasData(
                new Course() { ID = 1, Name = "CIS 400 Fall 2021" }
            );

            modelBuilder.Entity<Assignment>().ToTable("Assignment");
            modelBuilder.Entity<Assignment>()
                .HasDiscriminator<AssignmentType>("AssignmentType")
                .HasValue<Assignment>(AssignmentType.Assignment)
                .HasValue<CourseAssignment>(AssignmentType.CourseAssignment)
                .HasValue<MilestoneAssignment>(AssignmentType.MilestoneAssignment);

            modelBuilder.Entity<CourseAssignment>().ToTable("Assignment");
            modelBuilder.Entity<CourseAssignment>()
                .HasOne(a => a.Course)
                .WithMany(b => b.Assignments);

            modelBuilder.Entity<Assignment>().ToTable("Assignment");

            modelBuilder.Entity<IterativeProject>()
                .HasOne(a => a.Course)
                .WithMany(b => b.IterativeProjects)
                .OnDelete(DeleteBehavior.NoAction);            

            modelBuilder.Entity<MilestoneAssignment>().ToTable("Assignment");
            modelBuilder.Entity<MilestoneAssignment>()
                .HasOne(a => a.IterativeProject)
                .WithMany(b => b.Milestones)
                .OnDelete(DeleteBehavior.NoAction); 

            modelBuilder.Entity<ValidationAttempt>().ToTable("ValidationAttempt");
            modelBuilder.Entity<ValidationAttempt>().Property(p => p.StructuralIssues)
                .HasConversion(
                    v => JsonSerializer.Serialize(v, null),
                    v => JsonSerializer.Deserialize<List<ValidationIssue>>(v, null),
                    new ValueComparer<List<ValidationIssue>>(
                        (c1, c2) => c1.SequenceEqual(c2),
                        c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
                        c => c.ToList()));
            modelBuilder.Entity<ValidationAttempt>().Property(p => p.FunctionalIssues)
                .HasConversion(
                    v => JsonSerializer.Serialize(v, null),
                    v => JsonSerializer.Deserialize<List<ValidationIssue>>(v, null),
                    new ValueComparer<List<ValidationIssue>>(
                        (c1, c2) => c1.SequenceEqual(c2),
                        c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
                        c => c.ToList()));
            modelBuilder.Entity<ValidationAttempt>().Property(p => p.DesignIssues)
                .HasConversion(
                    v => JsonSerializer.Serialize(v, null),
                    v => JsonSerializer.Deserialize<List<ValidationIssue>>(v, null),
                    new ValueComparer<List<ValidationIssue>>(
                        (c1, c2) => c1.SequenceEqual(c2),
                        c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
                        c => c.ToList()));
            modelBuilder.Entity<ValidationAttempt>().Property(p => p.StyleIssues)
                .HasConversion(
                    v => JsonSerializer.Serialize(v, null),
                    v => JsonSerializer.Deserialize<List<ValidationIssue>>(v, null),
                    new ValueComparer<List<ValidationIssue>>(
                        (c1, c2) => c1.SequenceEqual(c2),
                        c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
                        c => c.ToList()));

            modelBuilder.Entity<ValidationAttempt>()
                .HasOne(a => a.User)
                .WithMany(b => b.ValidationAttempts);
            modelBuilder.Entity<ValidationAttempt>()
                .HasOne(a => a.Assignment)
                .WithMany(b => b.ValidationAttempts);

            modelBuilder.Entity<DiagnosticCode>().ToTable("DiagnosticCode");
            modelBuilder.Entity<DiagnosticCodeLookup>().ToTable("DiagnosticCodeCheck");
            modelBuilder.Entity<DiagnosticCodeLookup>()
                 .HasOne(a => a.User)
                 .WithMany(b => b.DiagnosticCodeLookups);
            modelBuilder.Entity<DiagnosticCodeLookup>()
                 .HasOne(a => a.DiagnosticCode)
                 .WithMany(b => b.DiagnosticCodeLookups);


            modelBuilder.Entity<GitHubAccount>().ToTable("GitHubAccount");
            modelBuilder.Entity<GitHubAccount>()
                .HasOne(a => a.User)
                .WithOne(b => b.GitHubAccount);
        }


    }
}
