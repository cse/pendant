using Microsoft.Build.Locator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using KSU.CS.ProgramVerifier;

namespace KSU.CS.Pendant.Server.Pages
{
    public class ProgramCheckModel : PageModel
    {
        public List<string> Issues { get; protected set; } 

        public async Task<ActionResult> OnGet()
        {
            var result = await Verifier.Check(@"D:\cis400\FinalGrader\CIS400FinalS2021\CIS400FinalS2021.sln", @"D:\cis400\Summer2021\milestone1");
            
            Issues = result.Issues;

            return Page();
        }

        //public async Task<ActionResult> OnPost(int assignmentID, IFormFile archive)
        //{ }
    }
}
