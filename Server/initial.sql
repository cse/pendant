Build started...
Build succeeded.
The Entity Framework tools version '5.0.2' is older than that of the runtime '5.0.7'. Update the tools for the latest features and bug fixes.
warn: Microsoft.EntityFrameworkCore.Model.Validation[10620]
      The property 'Issues.ValidationAttempt' is a collection or enumeration type with a value converter but with no value comparer. Set a value comparer to ensure the collection/enumeration elements are compared correctly.
IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [User] (
    [ID] int NOT NULL IDENTITY,
    [EID] nvarchar(max) NULL,
    [LastName] nvarchar(max) NULL,
    [FirstName] nvarchar(max) NULL,
    [IsStudent] bit NOT NULL,
    [IsFaculty] bit NOT NULL,
    [IsAdmin] bit NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY ([ID])
);
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ID', N'EID', N'FirstName', N'IsAdmin', N'IsFaculty', N'IsStudent', N'LastName') AND [object_id] = OBJECT_ID(N'[User]'))
    SET IDENTITY_INSERT [User] ON;
INSERT INTO [User] ([ID], [EID], [FirstName], [IsAdmin], [IsFaculty], [IsStudent], [LastName])
VALUES (1, N'nhbean', N'Nathan', CAST(0 AS bit), CAST(0 AS bit), CAST(0 AS bit), N'Bean');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ID', N'EID', N'FirstName', N'IsAdmin', N'IsFaculty', N'IsStudent', N'LastName') AND [object_id] = OBJECT_ID(N'[User]'))
    SET IDENTITY_INSERT [User] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ID', N'EID', N'FirstName', N'IsAdmin', N'IsFaculty', N'IsStudent', N'LastName') AND [object_id] = OBJECT_ID(N'[User]'))
    SET IDENTITY_INSERT [User] ON;
INSERT INTO [User] ([ID], [EID], [FirstName], [IsAdmin], [IsFaculty], [IsStudent], [LastName])
VALUES (2, N'russfeld', N'Russ', CAST(0 AS bit), CAST(0 AS bit), CAST(0 AS bit), N'Feldhausen');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ID', N'EID', N'FirstName', N'IsAdmin', N'IsFaculty', N'IsStudent', N'LastName') AND [object_id] = OBJECT_ID(N'[User]'))
    SET IDENTITY_INSERT [User] OFF;
GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ID', N'EID', N'FirstName', N'IsAdmin', N'IsFaculty', N'IsStudent', N'LastName') AND [object_id] = OBJECT_ID(N'[User]'))
    SET IDENTITY_INSERT [User] ON;
INSERT INTO [User] ([ID], [EID], [FirstName], [IsAdmin], [IsFaculty], [IsStudent], [LastName])
VALUES (3, N'weeser', N'Josh', CAST(0 AS bit), CAST(0 AS bit), CAST(0 AS bit), N'Weese');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ID', N'EID', N'FirstName', N'IsAdmin', N'IsFaculty', N'IsStudent', N'LastName') AND [object_id] = OBJECT_ID(N'[User]'))
    SET IDENTITY_INSERT [User] OFF;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210608214002_InitialCreate', N'5.0.7');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Assignment] (
    [ID] int NOT NULL IDENTITY,
    [Course] nvarchar(max) NULL,
    [Name] nvarchar(max) NULL,
    [SpecificationPath] nvarchar(max) NULL,
    CONSTRAINT [PK_Assignment] PRIMARY KEY ([ID])
);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210610025937_AddAssignments', N'5.0.7');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Assignment]') AND [c].[name] = N'Name');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Assignment] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Assignment] ALTER COLUMN [Name] nvarchar(max) NOT NULL;
ALTER TABLE [Assignment] ADD DEFAULT N'' FOR [Name];
GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Assignment]') AND [c].[name] = N'Course');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Assignment] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Assignment] ALTER COLUMN [Course] nvarchar(max) NOT NULL;
ALTER TABLE [Assignment] ADD DEFAULT N'' FOR [Course];
GO

CREATE TABLE [ValidationAttempt] (
    [ID] int NOT NULL IDENTITY,
    [UserID] int NULL,
    [AssignmentID] int NULL,
    [DateTime] datetime2 NOT NULL,
    [Issues] nvarchar(max) NULL,
    [SolutionPath] nvarchar(max) NULL,
    CONSTRAINT [PK_ValidationAttempt] PRIMARY KEY ([ID]),
    CONSTRAINT [FK_ValidationAttempt_Assignment_AssignmentID] FOREIGN KEY ([AssignmentID]) REFERENCES [Assignment] ([ID]) ON DELETE NO ACTION,
    CONSTRAINT [FK_ValidationAttempt_User_UserID] FOREIGN KEY ([UserID]) REFERENCES [User] ([ID]) ON DELETE NO ACTION
);
GO

CREATE INDEX [IX_ValidationAttempt_AssignmentID] ON [ValidationAttempt] ([AssignmentID]);
GO

CREATE INDEX [IX_ValidationAttempt_UserID] ON [ValidationAttempt] ([UserID]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210610165600_AddValidationAttempts', N'5.0.7');
GO

COMMIT;
GO


