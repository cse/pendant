﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace KSU.CS.Pendant.Server.Services
{
    public class GitHubService : IGitHubService
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _hostEnvironment;
        
        public GitHubService(IConfiguration configuration, IHostEnvironment hostEnvironment)
        {
            _configuration = configuration;
            _hostEnvironment = hostEnvironment;
        }

        public string CloneAndCheckout(string repoUrl, string commitId)
        {
            return CloneAndCheckoutAsync(repoUrl, commitId).GetAwaiter().GetResult();
        }

        public async Task<string> CloneAndCheckoutAsync(string repoUrl, string commitId)
        {
            var path = $"{ _configuration.GetValue<string>("Paths:HomeDir") }/{repoUrl}/{commitId}";
            
            var psi = new ProcessStartInfo()
            {
                FileName = "cmd.exe",
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            await File.AppendAllTextAsync("C:\\workDirectory\\log.txt", _hostEnvironment.EnvironmentName);
            await File.AppendAllTextAsync("C:\\workDirectory\\log.txt", $"\nIsProduction {_hostEnvironment.IsProduction()}\n");
            await File.AppendAllTextAsync("C:\\workDirectory\\log.txt", $"\nIsDevelopment {_hostEnvironment.IsDevelopment()}\n");

            // We need to explicitly set the user in the production environment to one with write permissions
            if (!_hostEnvironment.IsDevelopment())
            {
                await File.AppendAllTextAsync("C:\\workDirectory\\log.txt", "\nUsing user/password auth\n");
                var password = new SecureString();
                foreach (char c in "insecurepassword123") password.AppendChar(c);
                psi.UserName = "nhbean_local";
                #pragma warning disable CA1416 // Validate platform compatibility
                psi.Password = password;
                #pragma warning restore CA1416 // Validate platform compatibility
            }
            
            using var process = Process.Start(psi);

            using StreamWriter writer = process.StandardInput;

            
            if (writer.BaseStream.CanWrite)
            {
                await writer.WriteLineAsync($"whoami");                
                await writer.WriteLineAsync($"git clone git@github.com:{repoUrl}.git {path}");                
                await writer.WriteLineAsync($"cd {path}");
                await writer.WriteLineAsync($"git checkout {commitId}");
                await writer.WriteLineAsync("exit");
            }

            await process.WaitForExitAsync();

            var output = await process.StandardOutput.ReadToEndAsync();
            var errors = await process.StandardError.ReadToEndAsync();

            await File.AppendAllTextAsync("C:\\workDirectory\\log.txt", output);
            await File.AppendAllTextAsync("C:\\workDirectory\\log.txt", errors);

            return path;
        }
    }
}
