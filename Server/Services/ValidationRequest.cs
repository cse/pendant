﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Services
{
    public class ValidationRequest
    {
        public string RepoFullName { get; set; }

        public string CommitIdentifier { get; set; }

        public string SpecificationPath { get; set; }

        public int UserID { get; set; }

        public int AssignmentID { get; set; }

        public DateTime RequestedAt { get; set; }
    }
}
