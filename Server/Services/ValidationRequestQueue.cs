﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Services
{
    /// <summary>
    /// A threadsafe queue for enqueueing and dequeueing validation requests
    /// so that they can be processed in a background thread
    /// </summary>
    public class ValidationRequestQueue : IValidationRequestQueue
    {
        private readonly Channel<ValidationRequest> _queue;

        /// <summary>
        /// Creates a new ValidationRequestQueue
        /// </summary>
        /// <param name="capacity">The number of ValidationRequests the queue can hold</param>
        public ValidationRequestQueue(int capacity)
        {
            var options = new BoundedChannelOptions(capacity)
            {
                FullMode = BoundedChannelFullMode.Wait
            };
            _queue = Channel.CreateBounded<ValidationRequest>(options);
        }

        /// <summary>
        /// Enqueues a validation request for future processing
        /// </summary>
        /// <param name="validationRequest">The request to add to the queue</param>
        /// <returns>A task that resolves once the request is added to the queue</returns>
        public async ValueTask EnqueueAsync(ValidationRequest validationRequest)
        {
            if (validationRequest == null) throw new ArgumentNullException(nameof(validationRequest));
            await _queue.Writer.WriteAsync(validationRequest);
        }

        /// <summary>
        /// Removes the oldest ValidationRequest from the queue for processing
        /// </summary>
        /// <param name="cancellationToken">A cancellation token</param>
        /// <returns>A task resolving to the ValidationRequest</returns>
        public async ValueTask<ValidationRequest> DequeueAsync(CancellationToken cancellationToken)
        {
            var validationRequest = await _queue.Reader.ReadAsync(cancellationToken);
            return validationRequest;
        }
    }
}
