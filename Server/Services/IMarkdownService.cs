﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Services
{
    /// <summary>
    /// A service for processing Markdown
    /// </summary>
    public interface IMarkdownService
    {
        string ToHtml(string markdown);
    }
}
