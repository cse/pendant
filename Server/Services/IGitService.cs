﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Services
{
    public interface IGitHubService
    {
        string CloneAndCheckout(string repoUrl, string commitId);

        Task<string> CloneAndCheckoutAsync(string repoUrl, string commitId);
    }
}
