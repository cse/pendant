﻿using System;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;
using Microsoft.Extensions.Configuration;
using System.IO;
using LibGit2Sharp;
using System.IO.Compression;

namespace KSU.CS.Pendant.Server.Services
{
    public class SpecificationService : ISpecificationService 
    {
        private readonly DataContext _context;
        private readonly string _rootPath;

        public SpecificationService(DataContext context, IConfiguration configuration)
        {
            _context = context;
            _rootPath = configuration["Paths:HomeDir"];
        }

        #region Interface Methods
 
        /// <summary>
        /// Extracts and saves the <paramref name="assignment"/> specification into a local folder 
        /// and sets the path variable
        /// </summary>
        /// <param name="assignment">The assignment with the specification to save</param>
        /// <returns>True on success, false otherwise</returns>
        public bool SaveAssignmentSpecification(Assignment assignment)
        {
            return SaveAssignmentSpecificationAsync(assignment).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Extracts and saves the <paramref name="assignment"/> specification into a local folder 
        /// and sets the path variable
        /// </summary>
        /// <param name="assignment">The assignment with the specification to save</param>
        /// <returns>True on success, false otherwise</returns>
        public async Task<bool> SaveAssignmentSpecificationAsync(Assignment assignment)
        {
            // Save the assignment specification
            try
            {
                var tmpPath = Path.GetTempFileName();
                var randomName = Path.GetRandomFileName();
                var specificationPath = Path.Combine(_rootPath, "specifications", randomName);

                using (var stream = System.IO.File.Create(tmpPath))
                {
                    await assignment.Specification.CopyToAsync(stream);
                }

                await Task.Run(() => ZipFile.ExtractToDirectory(tmpPath, specificationPath, true));

                await Task.Run(() => System.IO.File.Delete(tmpPath));

                // The unzipped file adds an extra directory - should be archive.FileName with the .zip dropped
                var dirName = Path.GetFileNameWithoutExtension(assignment.Specification.FileName);
                assignment.SpecificationPath = Path.Combine(specificationPath, dirName);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        #endregion

    }
}
