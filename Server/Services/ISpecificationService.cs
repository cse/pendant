﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Services
{
    /// <summary>
    /// A service providing functionality related to validating student work
    /// </summary>
    public interface ISpecificationService
    {   
        /// <summary>
        /// Saves the supplied <paramref name="assignment"/>'s specification to 
        /// the file system.
        /// </summary>
        /// <param name="assignment">The assignment with the specification to save</param>
        /// <returns>true on success, false on failure</returns>
        public bool SaveAssignmentSpecification(Assignment assignment);

        /// <summary>
        /// Saves the supplied <paramref name="assignment"/>'s specification to 
        /// the file system.
        /// </summary>
        /// <param name="assignment">The assignment with the specification to save</param>
        /// <returns>true on success, false on failure</returns>
        public Task<bool> SaveAssignmentSpecificationAsync(Assignment assignment);
    }
}
