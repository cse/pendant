﻿using KSU.CS.Pendant.Server.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Services
{
    /// <summary>
    /// A service that processes ValidationRequests in the background and 
    /// creates and saves to the database the corresponding ValidationAttempt 
    /// </summary>
    public class ValidationRequestProcessingService : BackgroundService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IGitHubService _gitHubService;
        private readonly IValidationRequestQueue _queue;

        /// <summary>
        /// Consturcts a new instance of ValidationRequestProcessingService using dependency injection
        /// </summary>
        /// <param name="queue">The queue of validatoinRequests</param>
        /// <param name="context">The database context</param>
        /// <param name="gitHubService">A service for interacting with GitHub repositories</param>
        public ValidationRequestProcessingService(IValidationRequestQueue queue, IServiceScopeFactory scopeFactory, IGitHubService gitHubService)
        {
            _queue = queue;
            _scopeFactory = scopeFactory;
            _gitHubService = gitHubService;
        }

        /// <summary>
        /// Starts the background processing service
        /// </summary>
        /// <param name="cancellationToken">A cancellation token to signal a stop the service</param>
        /// <returns>A task that resolves once the background processing service has launched</returns>
        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            await BackgroundProcessing(cancellationToken);
        }

        /// <summary>
        /// Signals a stop to the background processing (which waits for current tasks to finish)
        /// </summary>
        /// <param name="cancellationToken">A cancellation token to signal a stop to the stop request</param>
        /// <returns>A task that resolves once the service stops</returns>
        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
        }

        /// <summary>
        /// The background processing loop
        /// </summary>
        /// <param name="cancellationToken">A cancellation token signalling a stop to background processing</param>
        /// <returns>A task that resolves once the background processing ends</returns>
        private async Task BackgroundProcessing(CancellationToken cancellationToken)
        {
            while(!cancellationToken.IsCancellationRequested)
            {
                var request = await _queue.DequeueAsync(cancellationToken);

                // Clone the supplied repository and check out the appropriate commit 
                var studentPath = await _gitHubService.CloneAndCheckoutAsync(request.RepoFullName, request.CommitIdentifier);

                // Validate the assignment 
                var result = await ProgramVerifier.Verifier.Check(studentPath, request.SpecificationPath);

                // Create the ValidationAttempt
                var attempt = new ValidationAttempt()
                {
                    UserID = request.UserID,
                    AssignmentID = request.AssignmentID,
                    RepoFullName = request.RepoFullName,
                    CommitIdentifier = request.CommitIdentifier,
                    DateTime = request.RequestedAt,
                    StructuralIssues = result.StructuralIssues.Select(issue => new ValidationIssue()
                    {
                        Message = issue
                        // TODO: add assignment url for HelpUrl
                    }).ToList(),
                    FunctionalIssues = result.FunctionalIssues.Select(issue => new ValidationIssue()
                    {
                        Message = issue
                    }).ToList(),
                    DesignIssues = result.DesignIssues.Select(issue => new ValidationIssue()
                    {
                        Message = issue.Message,
                        HelpUrl = issue.HelpUrl,
                        SourceUrl = BuildGitHubSourceUrl(request.RepoFullName, request.CommitIdentifier, issue.Path, issue.Line)
                    }).ToList(),
                    StyleIssues = result.StyleIssues.Select(issue => new ValidationIssue()
                    {
                        Message = issue.Message,
                        HelpUrl = issue.HelpUrl,
                        SourceUrl = BuildGitHubSourceUrl(request.RepoFullName, request.CommitIdentifier, issue.Path, issue.Line)
                    }).ToList()
                };

                // Save the validation attempt using a scoped data context
                using (var scope = _scopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                    context.ValidationAttempts.Add(attempt);
                    await context.SaveChangesAsync();
                }
            }
        }

        /// <summary>
        /// A helper method for building a link to display a specific line 
        /// in a specific file and release of a Git repository hosted on GitHub
        /// </summary>
        /// <param name="repoFullName">The full name of the repository, i.e. `kyle/test-stuff` </param>
        /// <param name="commitIdentifier">The commit hash</param>
        /// <param name="path">The path to the file within the repo</param>
        /// <param name="line">The line to display in that file</param>
        /// <returns>A URL to the desired location</returns>
        private static string BuildGitHubSourceUrl(string repoFullName, string commitIdentifier, string path, string line)
        {
            if (path.Length == 0) return "";
            // Build the souceURL 
            var sb = new StringBuilder();
            sb.Append("https://github.com/");
            sb.Append(repoFullName);
            sb.Append("/tree/");
            sb.Append(commitIdentifier);
            sb.Append(path);
            sb.Append("#L");
            sb.Append(line);
            return sb.ToString();
        }
    }
}
