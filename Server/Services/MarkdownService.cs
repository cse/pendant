﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Markdig;

namespace KSU.CS.Pendant.Server.Services
{
    /// <summary>
    /// A service for converting Markdown into HTML
    /// </summary>
    public class MarkdownService : IMarkdownService
    {
        private readonly MarkdownPipeline _pipeline = new MarkdownPipelineBuilder().UseAdvancedExtensions().Build();

        /// <summary>
        /// Converts the <paramref name="markdown"/> into a HTML string
        /// </summary>
        /// <param name="markdown">A string in markdown format</param>
        /// <returns>The equivalent HTML</returns>
        public string ToHtml(string markdown)
        {
            return Markdown.ToHtml(markdown, _pipeline);
        }
    }
}
