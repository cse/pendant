﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Services
{
    public interface IValidationRequestQueue
    {
        /// <summary>
        /// Adds a validation request to the queue to be processed at a future point
        /// </summary>
        /// <param name="request">The validation request to process</param>
        /// <returns>A task that resolves when the validation request is written to the queue</returns>
        ValueTask EnqueueAsync(ValidationRequest request);

        /// <summary>
        /// Removes a validation request from the queue for processing
        /// </summary>
        /// <returns>A ValueTask representing the validation request</returns>
        ValueTask<ValidationRequest> DequeueAsync(CancellationToken cancellationToken);
    }
}
