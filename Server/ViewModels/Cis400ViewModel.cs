﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.ViewModels
{
    public class Cis400ViewModel
    {
        /// <summary>The semester of this specific course offering</summary>
        //public Semester Semester { get; set; }

        /// <summary>The current user viewing the course</summary>
        public User User { get; set; }

        /// <summary>The semester project for this course</summary>
        public IterativeProject SemesterProject { get; set; }
    }
}
