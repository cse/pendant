﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.ViewModels
{
    public class IssueListViewModel
    {
        public bool Available { get; set; }

        public string Category { get; set; }

        public List<ValidationIssue> Issues { get; set; }

        public string Description { get; set; }
    }
}
