﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace KSU.CS.Pendant.Server.Helpers
{
    public class GithubHelper
    {
        public static async Task<string> CloneAndCheckout(string identifier, string repoUrl, string branch)
        {
            var regex = new Regex(@"github.com/([^/]+)/([^/]+)/");
            var matches = regex.Match(repoUrl);
            var gitUserName = matches.Groups[1].Value;
            var repoName = matches.Groups[2].Value;
            var tagName = matches.Groups[3].Value;

            var psi = new ProcessStartInfo() {
                FileName = "cmd.exe",
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            using var process = Process.Start(psi);

            using StreamWriter writer = process.StandardInput;
            
            if (writer.BaseStream.CanWrite)
            {
                writer.WriteLine($"git clone git@github.com:{gitUserName}/{repoName}.git D:/checkrepos/{identifier}");
                writer.WriteLine($"cd D:/checkrepos/{identifier}");
                writer.WriteLine($"git checkout tags/{branch}");
            }
            
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            await process.WaitForExitAsync();
            return $"D:/checkrepos/{identifier}";
        }
    }
}
