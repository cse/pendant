﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Controllers
{
    public class DashboardController : Controller
    {
        DataContext _context;


        public DashboardController(DataContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            // Load the current user's data
            User user = await _context.Users
                .Include(u => u.ValidationAttempts)
                .ThenInclude(v => v.Assignment)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            return StudentDashboard(user);
        }

        public IActionResult StudentDashboard(User user)
        {
            return StatusCode(503);
        }
    }
}
