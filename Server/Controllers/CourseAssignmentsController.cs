﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;
using KSU.CS.Pendant.Server.Services;
using Microsoft.AspNetCore.Authorization;

namespace KSU.CS.Pendant.Server.Controllers
{

    /// <summary>
    /// Provides the functionality for creating, viewing, updating and deleting CourseAssignments
    /// </summary>
    public class CourseAssignmentsController : Controller
    {
        protected readonly DataContext _context;
        protected readonly ISpecificationService _specificationService;

        /// <summary>
        /// Constructs a new CourseAssignmentsController using dependency injection
        /// </summary>
        /// <param name="context">The database context</param>
        /// <param name="specificationService">The assignment validation service</param>
        public CourseAssignmentsController(DataContext context, ISpecificationService specificationService)
        {
            _context = context;
            _specificationService = specificationService;
        }

        #region Route Methods

        /// <summary>
        /// Lists all created assignments
        /// </summary>
        /// <returns>A HTML page listing the assignemnts</returns>
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Index()
        {
            var assignments = _context.Assignments
                .ToList();

            return View(assignments);
        }

        /// <summary>
        /// Provides the form for creating a new assignment for the course with <paramref name="courseID"/>
        /// </summary>
        /// <param name="courseID">The database ID of the course to create an assignment for</param>
        /// <returns>A HTML page containing the form</returns>
        [Route("/Courses/{courseID}/Assignments/Create")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Create([FromRoute] int courseID)
        {
            var assignment = new CourseAssignment()
            {
                Course = await _context.Courses.FindAsync(courseID)
            };

            return View(assignment);
        }

        /// <summary>
        /// Adds a new assignment to the list of assignments.  
        /// </summary>
        /// <param name="assignment"></param>
        /// <param name="archive"></param>
        /// <returns>A redirect to the Index page, or the form if corrections are needed</returns>
        [HttpPost]
        [Route("/Courses/{courseID}/Assignments/Create")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Create(CourseAssignment assignment, [FromRoute] int courseID)
        {
            if (!ModelState.IsValid)
            {
                return View(assignment);
            }

            //assignment.CourseID = courseID;

            // Save the assignment specification to the file system
            var saved = await _specificationService.SaveAssignmentSpecificationAsync(assignment);
            if (!saved)
            {
                return StatusCode(500);
            }

            // Save the assignment to the database
            _context.CourseAssignments.Add(assignment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", "Courses", new { ID = courseID });
        }

        #endregion
    }
}
