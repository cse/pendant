﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;
using Microsoft.Extensions.Configuration;
using KSU.CS.Pendant.Server.Services;
using Microsoft.AspNetCore.Authorization;

namespace KSU.CS.Pendant.Server.Controllers
{
    /// <summary>
    /// A controller for creating, reading, updating, and deleting milestone assignments
    /// </summary>
    public class MilestonesController : Controller
    {
        private readonly DataContext _context;
        private readonly ISpecificationService _specificationService;

        /// <summary>
        /// Constructs a new MilestoneAssignmentController using dependency injection
        /// </summary>
        /// <param name="context">The database context for the site</param>
        /// <param name="specificationService">An assignment validation service</param>
        public MilestonesController(DataContext context, ISpecificationService specificationService) 
        {
            _context = context;
            _specificationService = specificationService;
        }

        #region Route Methods

        /// <summary>
        /// Creates a new milstone assignment for the course with ID of <paramref name="courseID"/> and 
        /// iterative project with ID of <paramref name="iterativeProjectID"/>
        /// </summary>
        /// <param name="courseID"></param>
        /// <param name="iterativeProjectID"></param>
        /// <returns></returns>
        [Authorize(Policy = "AdminOnly")]
        [Route("/Courses/{courseID}/IterativeProjects/{iterativeProjectID}/Milestones/Create")]
        public async Task<IActionResult> Create([FromRoute] int courseID, [FromRoute] int iterativeProjectID)
        {
            var assignment = new MilestoneAssignment()
            {
                IterativeProject = await _context.IterativeProjects.FindAsync(iterativeProjectID)
            };

            return View(assignment);
        }

        /// <summary>
        /// Adds a new assignment to the list of assignments.  
        /// </summary>
        /// <param name="assignment"></param>
        /// <param name="archive"></param>
        /// <returns>A redirect to the Index page, or the form if corrections are needed</returns>
        [HttpPost]
        [Route("/Courses/{courseID}/IterativeProjects/{iterativeProjectID}/Milestones/Create")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Create(MilestoneAssignment assignment, [FromRoute] int courseID, [FromRoute] int iterativeProjectID)
        {
            if (!ModelState.IsValid)
            {
                return View(assignment);
            }

            // Save the assignment specification to the filesystem
            var saved = await _specificationService.SaveAssignmentSpecificationAsync(assignment);
            if (!saved)
            {
                return StatusCode(500);
            }

            // Save the assignment to the database
            _context.MilestoneAssignments.Add((MilestoneAssignment)assignment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", "Courses", new { ID = courseID });
        }

        [Route("/Courses/{courseID}/IterativeProjects/{iterativeProjectID}/Milestones/Update/{milestoneID}")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Update([FromRoute] int milestoneID, [FromRoute] int courseID, [FromRoute] int iterativeProjectID)
        {
            var assignment = await _context.MilestoneAssignments.FindAsync(milestoneID);

            return View(assignment);
        }

        /// <summary>
        /// Adds a new assignment to the list of assignments.  
        /// </summary>
        /// <param name="assignment"></param>
        /// <param name="archive"></param>
        /// <returns>A redirect to the Index page, or the form if corrections are needed</returns>
        [HttpPost]
        [Route("/Courses/{courseID}/IterativeProjects/{iterativeProjectID}/Milestones/Update/{milestoneID}")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Update(MilestoneAssignment assignment, [FromRoute] int milestoneID, [FromRoute] int courseID, [FromRoute] int iterativeProjectID)
        {
            if (!ModelState.IsValid)
            {
                return View(assignment);
            }

            // Save the assignment specification to the filesystem
            var saved = await _specificationService.SaveAssignmentSpecificationAsync(assignment);
            if (!saved)
            {
                return StatusCode(500);
            }

            // Save the assignment to the database
            _context.MilestoneAssignments.Update((MilestoneAssignment)assignment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", "Courses", new { ID = courseID });
        }

        #endregion 
    }
}
