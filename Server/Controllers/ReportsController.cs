﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Controllers
{
    public class ReportsController : Controller
    {
        private readonly DataContext _context;

        /// <summary>
        /// Constructs a new CourseController using dependency injection
        /// </summary>
        /// <param name="context">The site database context</param>
        public ReportsController(DataContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Summary()
        {
            var users = await _context.Users
                .Include(u => u.DiagnosticCodeLookups)
                .Include(u => u.ValidationAttempts)
                .ThenInclude(u => u.Assignment)
                .Where(u => u.Roles == Models.UserRoles.Student)
                .OrderBy(u => u.LastName)
                .ToListAsync();

            return View(users);
        }

        /// <summary>
        /// A page offering a report of diagnostic codes found in validations
        /// </summary>        
        /// <returns>A HTMPL page describing the issue corresponding to the diagonstic code</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> DiagnosticCounts()
        {
            // Get all Diagnostic Codes
            IEnumerable<DiagnosticCode> diagnosticCode = await _context.DiagnosticCodes
                .Include(dc => dc.DiagnosticCodeLookups)
                .ToListAsync();

            // Get all Validations and thier issues 
            var validations = await _context.ValidationAttempts.ToListAsync();

            // Collect the counts for each issue
            Dictionary<DiagnosticCode, int> diagnosticCounts = new();
            foreach(var diagnostic in diagnosticCode)
            {
                diagnosticCounts[diagnostic] = 0;
                foreach(var validation in validations)
                {
                    foreach(var issue in validation.DesignIssues)
                    {
                        if (issue.HelpUrl.Contains(diagnostic.Code))
                        {
                            diagnosticCounts[diagnostic]++;
                        }
                    }
                    foreach (var issue in validation.StyleIssues)
                    {
                        if (issue.HelpUrl.Contains(diagnostic.Code))
                        {
                            diagnosticCounts[diagnostic]++;
                        }
                    }
                }
            }

            return View(diagnosticCounts);
        }
    }
}
