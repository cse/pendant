﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Controllers
{
    /// <summary>
    /// A controller for creating, viewing, updating, and deleting franchises
    /// used for the CIS 400 semester-long project
    /// </summary>
    public class FranchisesController : Controller
    {
        private readonly DataContext _context;

        /// <summary>
        /// Constructs a new CourseController using dependency injection
        /// </summary>
        /// <param name="context">The site database context</param>
        public FranchisesController(DataContext context)
        {
            _context = context;
        }

        #region Route Methods

        /// <summary>
        /// Lists the available franchises
        /// </summary>
        /// <returns>A HTML page listing the franchises</returns>
        public async Task<IActionResult> Index()
        {
            var user = await _context.Users
                .Include(u => u.FranchiseLocation)
                .ThenInclude(l => l.OrderRecords)
                .Where(u => u.ID == int.Parse(User.FindFirst("ID").Value))
                .FirstAsync();

//            if (user.IsAdmin || user.IsFaculty)
//            {
//                var franchises = await _context.FranchiseLocations.ToListAsync();
//                return View(franchises);
//            }
//            else
            {
                // Create the Franchise Location for this user if it
                // does not yet exist
                if (user.FranchiseLocation is null)
                {
                    user.FranchiseLocation = new FranchiseLocation()
                    {
                        Name = $"{user.FullName}'s Franchise"
                    };
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                return View("Details", user.FranchiseLocation);
            }
        }

        /// <summary>
        /// A page giving details of the selected franchise with <paramref name="id"/>
        /// </summary>
        /// <param name="id">The franchise database ID</param>
        /// <returns>A HTML page with the details</returns>
        public async Task<IActionResult> Details(int id)
        {
            var franchise = await _context.FranchiseLocations
                .Where(f => f.ID == id)
                .FirstOrDefaultAsync();

            if (franchise is null) return NotFound();

            return View(franchise);
        }
        public async Task<IActionResult> Update(FranchiseLocation franchise)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Save the Franchise
            _context.FranchiseLocations.Update(franchise);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", new { ID = franchise.ID });
        }

        /// <summary>
        /// Regenerates the user's secret to use with the GitHub Webhooks
        /// </summary>
        /// <returns>A HTTP redirect</returns>
        [Route("RegenerateSecret")]
        public async Task<IActionResult> RegenerateSecret()
        {
            // Load the current user's data
            User user = await _context.Users
                .Include(u => u.FranchiseLocation)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            // The user must have a connected franchise location
            if (user.FranchiseLocation == null) return NotFound();

            // Regenerate the user's secret 
            user.FranchiseLocation.GenerateSecret();

            // Save the change 
            _context.Update(user.FranchiseLocation);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", new { ID = user.FranchiseLocation.ID });
        }

        #endregion
    }
}
