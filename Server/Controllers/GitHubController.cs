﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using KSU.CS.Pendant.Server.Models;
using System.Text.Json.Serialization;
using KSU.CS.Pendant.Server.Services;

namespace KSU.CS.Pendant.Server.Controllers
{
    /// <summary>
    /// A Controller for managing GitHub-related requests, primarily centered around:
    /// 1. Connecting a user account from this site to a GitHub account 
    /// 2. Processing webhooks triggered by those GitHub users
    /// </summary>
    [Route("GitHub")]
    public class GitHubController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly DataContext _context;
        private readonly IGitHubService _gitHubService;
        private readonly IValidationRequestQueue _validationRequestQueue;
        private readonly string _redirectURI;
        private readonly string _clientID;
        private readonly string _clientSecret;

        #region Helper Classes

        /// <summary>
        /// A class to deserialize the GitHub API OAuth validation response into
        /// </summary>
        private class GitHubAuthResponse
        {
            [JsonPropertyName("access_token")]
            public string AccessToken { get; set; }

            [JsonPropertyName("scope")]
            public string Scope { get; set; }

            [JsonPropertyName("token_type")]
            public string TokenType { get; set; }
        }

        /// <summary>
        /// A class to deserialze the GitHub API /users response into
        /// </summary>
        internal class GitHubUserResponse
        {
            [JsonPropertyName("login")]
            public string Username { get; set; }

            public Dictionary<string, JsonElement> ExtensionData { get; set; }
        }

        /// <summary>
        /// A class to deserialize a GitHub Webhook payload
        /// </summary>
        internal class GitHubWebhookPayload
        {
            internal class GitHubRepository
            {
                [JsonPropertyName("name")]
                public string Name { get; set; }

                [JsonPropertyName("full_name")]
                public string FullName { get; set; }

                public Dictionary<string, JsonElement> ExtensionData { get; set; }
            }

            internal class GitHubSender
            {
                [JsonPropertyName("login")]
                public string Name { get; set; }

                public Dictionary<string, JsonElement> ExtensionData { get; set; }
            }

            [JsonPropertyName("ref")]
            public string Ref { get; set; }

            [JsonPropertyName("after")]
            public string CommitID { get; set; }

            [JsonPropertyName("repository")]
            public GitHubRepository Repository { get; set; }

            [JsonPropertyName("sender")]
            public GitHubSender Sender { get; set; }

            public Dictionary<string, JsonElement> ExtensionData { get; set; }
        }

        #endregion

        /// <summary>
        /// Constructs a new GitHub controller
        /// </summary>
        /// <param name="dataContext">The data context for the site</param>
        /// <param name="configuration">The website configuration</param>
        /// <param name="httpClientFactory">A factory for creating shared HTTP clients</param>
        public GitHubController(DataContext dataContext, IConfiguration configuration, IHttpClientFactory httpClientFactory, IGitHubService gitHubService, IValidationRequestQueue validationRequestQueue, ISpecificationService assignmentValidationService)
        {
            _context = dataContext;
            _gitHubService = gitHubService;
            _validationRequestQueue = validationRequestQueue;
            _redirectURI = $"{configuration["GitHub:ServiceHost"]}Authorize";
            _clientID = configuration["GitHub:ClientID"];
            _clientSecret = configuration["GitHub:ClientSecret"];
            _httpClientFactory = httpClientFactory;
        }


        #region Route Methods

        /// <summary>
        /// Displays the GitHub info for the connected user
        /// </summary>
        /// <returns>A HTML page with the info</returns>
        [Route("")]
        public async Task<IActionResult> Index()
        {
            // Load the current user's data
            User user = await _context.Users
                .Include(u => u.GitHubAccount)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            if (user.IsStudent) return View("Details", user.GitHubAccount);

            if (!(user.IsAdmin || user.IsFaculty)) return StatusCode(403);

            var accounts = await _context.GitHubAccounts
                .Include(a => a.User)
                .ToListAsync();

            return View(accounts);
        }

        /// <summary>
        /// Displays the details page for the GitHubAccount with <paramref name="id"/>
        /// </summary>
        /// <param name="id">The database id for the account</param>
        /// <returns>A page with the details</returns>
        [Authorize(Policy = "AdminOnly")]
        [Route("GitHubAccount/Details/{id}")]
        public async Task<IActionResult> Details(int id)
        {
            var gitHubAccount = await _context.GitHubAccounts
                .Include(a => a.User)
                .Where(a => a.ID == id)
                .FirstOrDefaultAsync();

            if (gitHubAccount is null) return StatusCode(404, "GitHub Account not found");

            return View(gitHubAccount);
        }


        /// <summary>
        /// Removes this GitHub account from the user
        /// </summary>
        /// <returns>A HTTP redirect</returns>
        [Route("GitHubAccount/Delete/{id}")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Delete(int id)
        {
            // Load the current user's data
            var gitHubAccount = await _context.GitHubAccounts.FindAsync(id);

            // Remove the GitHub account from the user (if it exists)
            if (gitHubAccount != null)
            {
                _context.Remove(gitHubAccount);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }


        /// <summary>
        /// Regenerates the user's secret to use with the GitHub Webhooks
        /// </summary>
        /// <returns>A HTTP redirect</returns>
        [Route("RegenerateSecret")]
        public async Task<IActionResult> RegenerateSecret()
        {
            // Load the current user's data
            User user = await _context.Users
                .Include(u => u.GitHubAccount)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            // The user must have a connected GitHub account
            if (user.GitHubAccount == null) return RedirectToAction("Connect");

            // Regenerate the user's secret 
            user.GitHubAccount.GenerateSecret();

            // Save the change 
            _context.Update(user.GitHubAccount);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Redirects the user to GitHub OAuth to start the account linking process
        /// </summary>
        /// <returns>A HTTP redirect</returns>
        [Route("Connect")]
        public IActionResult Connect()
        {
            var scope = ""; // currently we don't use the API for anything but requesting public user info
            var state = System.Guid.NewGuid().ToString(); // state is used between the initial request and validation request to prevent cross-site attacks
            HttpContext.Session.SetString("GitHubState", state);
            return Redirect($"https://github.com/login/oauth/authorize?client_id={_clientID}&redirect_uri={_redirectURI}&scope={scope}&state={state}");
        }

        /// <summary>
        /// Removes this GitHub account from the user
        /// </summary>
        /// <returns>A HTTP redirect</returns>
        [Route("Disconnect")]
        public async Task<IActionResult> Disconnect()
        {
            // Load the current user's data
            User user = await _context.Users
                .Include(u => u.GitHubAccount)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            // Remove the GitHub account from the user (if it exists)
            if (!(user.GitHubAccount is null))
            {
                _context.Remove(user.GitHubAccount);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }


        /// <summary>
        /// Handles the redirect from GitHub OAuth and validates the provided <paramref name="code"/> and <paramref name="state"/>
        /// </summary>
        /// <param name="code">The code provided by GitHub as part of the OAuth validation process</param>
        /// <param name="state">The state passed along with the initial request to detect and avoid cross-site attacks</param>
        /// <returns>Either a page with the connected GitHub account info, or an HTTP error status code</returns>
        [Route("Authorize")]
        public async Task<IActionResult> Authorize([FromQuery] string code, [FromQuery] string state)
        {
            // Check for cross-site request attacks
            string savedState = HttpContext.Session.GetString("GitHubState");
            if (state != savedState) return StatusCode(403);

            // Validate the provided OAuth Code
            var authResponse = await ValidateOAuthCode(code);
            if (authResponse is null) return StatusCode(403);

            // Get the connected GitHub User data
            var userResponse = await FetchGitHubUserData(authResponse.AccessToken);
            if (userResponse is null) return StatusCode(500);

            // Load the current site user's data
            User user = await _context.Users
                .Include(u => u.GitHubAccount)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            if (user.GitHubAccount is null)
            {
                // Add a GitHub account 
                user.GitHubAccount = new GitHubAccount()
                {
                    AccessToken = authResponse.AccessToken,
                    Secret = System.Guid.NewGuid().ToString(),
                    Username = userResponse.Username
                };
            }
            else
            {
                // Update the GitHub account
                user.GitHubAccount.Username = userResponse.Username;
                user.GitHubAccount.AccessToken = authResponse.AccessToken;
            }

            await _context.SaveChangesAsync();
            return View( "Details", user.GitHubAccount);
        }


        /// <summary>
        /// Processes incoming webhook requests from GitHub for assigment series milestones,
        /// creating and queueing a validation request to be run as soon as possible.
        /// </summary>
        /// <returns>An HTTP status code reflecting the outcome</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("MilestoneWebhook")]
        public async Task<IActionResult> MilestoneWebhook()
        {
            // Get the request signature
            Request.Headers.TryGetValue("X-Hub-Signature-256", out var requestHash);

            // Read in the raw body as a string 
            using var reader = new System.IO.StreamReader(Request.Body, Encoding.UTF8);
            var body = await reader.ReadToEndAsync();

            // Check for X-WWW-Form-Encoded 
            if (body.StartsWith("payload")) return StatusCode(400, "You must select 'application/json' as the Content Type for your webhook");

            // Deserialize the payload            
            var payload = ParseGitHubWebhookPayload(body);
            if(payload is null) return StatusCode(400, "Error parsing JSON request body.");

            // Determine the github user and associated site user
            var gitHubUserName = payload.Sender.Name;
            var gitHubAccount = await _context.GitHubAccounts
                .Include(g => g.User)
                .Where(g => g.Username == gitHubUserName)
                .FirstOrDefaultAsync();
            if (gitHubAccount is null) return StatusCode(401, $"The GitHub user {gitHubUserName} is not set up to use this service.  Please connect your GitHub account to Pendant");

            // Compute the signature from the request body
            var computedHash = HmacSHA256(gitHubAccount.Secret, body);

            // Verify the signatures match
            var match = computedHash == requestHash;
            if (!match) return StatusCode(403, "Signature does not match - You may need to update your secret in your GitHub repo's webhook settings.");

            // If this was a ping request, send a 200 response
            Request.Headers.TryGetValue("X-GitHub-Event", out var gitHubEvent);
            if (gitHubEvent == "ping") return StatusCode(200);

            // If this was anything else than a push, send a 501 response 
            if (gitHubEvent != "push") return StatusCode(501, "Only ping and push events are currently supported");
                        
            // Determine the branch 
            var refPath = payload.Ref;
            var branch = (new Regex(@"refs/heads/([^/]+)")).Match(refPath).Groups[1].Value;

            // Determine the repository 
            var repo = payload.Repository.Name;

            // Determine the original project repo name (strip the GitHub User Name) 
            var usernameIndex = repo.IndexOf(gitHubAccount.Username);
            var projectRepoPrefix = usernameIndex == -1 ? repo : repo.Substring(0, repo.Length - gitHubAccount.Username.Length - 1);

            // Find the matching iterative project
            var project = await _context.IterativeProjects
                .Include(p => p.Milestones)
                .FirstOrDefaultAsync(a => a.RepositoryPrefix == projectRepoPrefix);
            if (project is null) return StatusCode(404, $"The project {projectRepoPrefix} is not an iterative project validated by Pendant");

            // Find the matching milestone
            var milestone = project.Milestones
                .Where(ms => ms.BranchName == branch)
                .FirstOrDefault();
            if (milestone is null)
            {
                var milestones = String.Join(", ", project.Milestones.Select(m => m.BranchName));
                return StatusCode(404, $"The branch {branch} does not match one of the expected milestone branches: {milestones}");
            }

            // Add the request to the queue to be processed
            await _validationRequestQueue.EnqueueAsync(new ValidationRequest()
            {
                AssignmentID = milestone.ID,
                UserID = gitHubAccount.UserID,
                SpecificationPath = milestone.SpecificationPath,
                RepoFullName = payload.Repository.FullName,
                CommitIdentifier = payload.CommitID,
                RequestedAt = DateTime.Now
            });

            // Return a success message
            return StatusCode(200, $"Validation request added to the queue");            
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Parses a webhook json payload into C# objects
        /// </summary>
        /// <param name="body">The JSON to parse</param>
        /// <returns>The payload as a C# object</returns>
        private GitHubWebhookPayload ParseGitHubWebhookPayload(string body)
        {
            try
            {
                return JsonSerializer.Deserialize<GitHubWebhookPayload>(body);
            } 
            catch(Exception e) 
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// Validates the provided OAuth <paramref name="code"/> for the GitHub API
        /// </summary>
        /// <param name="code">An OAuth code to validate</param>
        /// <returns>The validation response, or null on a failure</returns>
        private async Task<GitHubAuthResponse> ValidateOAuthCode(string code)
        {
            var url = $"https://github.com/login/oauth/access_token?client_id={_clientID}&client_secret={_clientSecret}&code={code}&redirect_uri={_redirectURI}";
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Headers.Add("User-Agent", "Pendant");
            request.Headers.Add("Accept", "application/json");

            var client = _httpClientFactory.CreateClient();
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode) return null;

            try
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                var authResponse = await JsonSerializer.DeserializeAsync<GitHubAuthResponse>(responseStream);
                return authResponse;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Fetches the login for the GitHub user associated with the provided <paramref name="accessToken"/>
        /// </summary>
        /// <param name="accessToken">An access token for the GitHub API</param>
        /// <returns>An object containing the GitHub username, or null on a failure</returns>
        private async Task<GitHubUserResponse> FetchGitHubUserData(string accessToken)
        {
            // Determine the GitHub account's username
            var client = _httpClientFactory.CreateClient();
            var url = "https://api.github.com/user";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("User-Agent", "Pendant");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", $"token {accessToken}");
            var newResponse = await client.SendAsync(request);

            if (!newResponse.IsSuccessStatusCode) return null;
            {
                using var responseStream = await newResponse.Content.ReadAsStreamAsync();
                var userResponse = await JsonSerializer.DeserializeAsync<GitHubUserResponse>(responseStream);
                return userResponse;
            }
        }

        /// <summary>
        /// Generates a HMAC SHA 256 hash signature using the provided <paramref name="secret"/> and <paramref name="body"/>
        /// </summary>
        /// <param name="secret">The encryption key to use</param>
        /// <param name="body">The data to create a signature for</param>
        /// <returns>The hash signature</returns>
        private static string HmacSHA256(string secret, string body)
        {
            byte[] code = Encoding.ASCII.GetBytes(secret);
            using var hmac = new HMACSHA256(code);
            var hmBytes = hmac.ComputeHash(Encoding.ASCII.GetBytes(body));
            StringBuilder builder = new();
            builder.Append("sha256=");
            foreach (byte b in hmBytes)
            {
                builder.Append(b.ToString("x2"));
            }
            return builder.ToString();
        }

        #endregion
    }
}
