﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace KSU.CS.Pendant.Server
{
    /// <summary>
    /// A controller for creating, viewing, updating, and destroying iterative projects
    /// </summary>
    public class IterativeProjectsController : Controller
    {
        private readonly DataContext _context;

        /// <summary>
        /// Constructs a new IterativeProjectsController using dependency injection
        /// </summary>
        /// <param name="context">The database context for the site</param>
        public IterativeProjectsController(DataContext context)
        {
            _context = context;
        }

        #region Route Methods

        /// <summary>
        /// Lists all iterative projects in the database
        /// </summary>
        /// <returns>A HTML page listing the iterative projects</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Index()
        {
            var iterativeAssignment = await _context.IterativeProjects.ToListAsync();

            return View(iterativeAssignment);
        }

        [Route("/Courses/{courseID}/IterativeProjects/Create")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Create([FromRoute] int courseID)
        {
            var iterativeProject = new IterativeProject()
            {
                Course = await _context.Courses.FindAsync(courseID)
            };

            return View(iterativeProject);
        }

        /// <summary>
        /// Adds a new assignment to the list of assignments.  
        /// </summary>
        /// <param name="assignment"></param>
        /// <param name="archive"></param>
        /// <returns>A redirect to the Index page, or the form if corrections are needed</returns>
        [HttpPost]
        [Route("/Courses/{courseID}/IterativeProjects/Create")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Create(IterativeProject iterativeProject, [FromRoute] int courseID)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            _context.IterativeProjects.Add(iterativeProject);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", "Courses", new { ID = courseID });
        }

        #endregion
    }
}
