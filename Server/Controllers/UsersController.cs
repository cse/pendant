﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Controllers
{
    public class UsersController : Controller
    {
        DataContext _context; 

        /// <summary>
        /// Constructs a new UsersController using dependency injection
        /// </summary>
        /// <param name="context"></param>
        public UsersController(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Lists all users in the system
        /// </summary>
        /// <returns>A HTML page listing the assignemnts</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Index()
        {
            var users = await _context.Users.ToListAsync();

            return View(users);
        }

        /// <summary>
        /// Displays the user details
        /// </summary>
        /// <param name="id">The id of the user</param>
        /// <returns>A HTML page with the details</returns>
        public async Task<IActionResult> Details(int id)
        {
            var user = await _context.Users
                .Where(user => user.ID == id)
                .Include(user => user.GitHubAccount)
                .Include(user => user.ValidationAttempts.OrderByDescending(va => va.DateTime))
                .FirstOrDefaultAsync();

            return View("Details", user);
        }

        /// <summary>
        /// Provides a form for editing users
        /// </summary>
        /// <returns>A HTML form for editing users</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await _context.Users.FindAsync(id);

            return View("Update", user);
        }

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <returns>A HTML form for editing users</returns>
        [HttpPost]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Update(User user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            _context.Update(user);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Deletes the user
        /// </summary>
        /// <returns>A HTML redirect</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Destroy(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user is null) return StatusCode(404);

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
