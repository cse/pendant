﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using KSU.CS.Pendant.Server.Models;
using KSU.CS.Pendant.Server.Services;

namespace KSU.CS.Pendant.Server.Controllers
{
    /// <summary>
    /// A controller for creating, viewing, updating, and deleting descriptions of diagnostic codes
    /// </summary>
    public class DiagnosticCodesController : Controller
    {
        private readonly DataContext _context;
        private readonly IMarkdownService _markdown;

        /// <summary>
        /// Constructs a new DiagnosticCodesController using dependency injection
        /// </summary>
        /// <param name="context">The database context for the site</param>
        public DiagnosticCodesController(DataContext context, IMarkdownService markdown)
        {
            _context = context;
            _markdown = markdown;
        }

        #region Route Methods

        /// <summary>
        /// Lists all diagnostic codes currently in the system
        /// </summary>
        /// <returns>A HTML page listing the diagnostic codes</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Index()
        {
            var codes = await _context.DiagnosticCodes.OrderBy(c => c.Code).ToListAsync();

            return View(codes);
        }

        /// <summary>
        /// Gives the details for a specific diagnostic code, and logs the request
        /// for long-term tracking of use of the pedagogic function of the site
        /// </summary>
        /// <param name="code">The diagnostic code</param>
        /// <returns>A HTML page describing the diagnosed issue</returns>
        [Route("Diagnostic/{code}")]
        public async Task<IActionResult> Details(string code)
        {
            // Load the current user's data
            User user = await _context.Users
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            // Find the Diagnostic Code 
            DiagnosticCode diagnosticCode = await _context.DiagnosticCodes
                .FirstAsync(c => c.Code == code);

            if (diagnosticCode is null) return NotFound();

            // Save the code lookup 
            _context.DiagnosticCodeLookups.Add(new DiagnosticCodeLookup()
            {
                User = user,
                DiagnosticCode = diagnosticCode
            });
            await _context.SaveChangesAsync();

            return View("Details", diagnosticCode);
        }

        /// <summary>
        /// A page offering the details of the diagnostic code with <paramref name="id"/>
        /// </summary>
        /// <param name="id">The code's database id</param>
        /// <returns>A HTMPL page describing the issue corresponding to the diagonstic code</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Details(int id)
        {
            // Find the Diagnostic Code 
            DiagnosticCode diagnosticCode = await _context.DiagnosticCodes.FindAsync(id);

            if (diagnosticCode is null) return NotFound();

            return View(diagnosticCode);
        }

        /// <summary>
        /// Provides the form for creating a new diagnostic code
        /// </summary>
        /// <returns>A HTML page containing a form</returns>
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Creates a new diagnostic code corresponding to <paramref name="diagnosticCode"/>
        /// </summary>
        /// <param name="diagnosticCode"></param>
        /// <returns>A redirect to the details page on success, or the form on a failure</returns>
        [HttpPost]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Create(DiagnosticCode diagnosticCode)
        {
            var valid = ModelState.IsValid;

            // Process the Markdown into HTML
            if (diagnosticCode.Markdown != null) diagnosticCode.Html = _markdown.ToHtml(diagnosticCode.Markdown);

            // Validate the resulting DiagnosticCode 
            ModelState.ClearValidationState(nameof(DiagnosticCode));
            if (!TryValidateModel(diagnosticCode, nameof(DiagnosticCode)))
            {
                return View(diagnosticCode);
            }

            // Save the DiagnosticCode
            _context.DiagnosticCodes.Add(diagnosticCode);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", new { ID = diagnosticCode.ID });
        }

        /// <summary>
        /// Provides an edit form for an existing diagnostic code
        /// </summary>
        /// <param name="id">The database id of the diagnostic code</param>
        /// <returns>A HTML form</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Edit(int id)
        {
            var diagnosticCode = await _context.DiagnosticCodes.FindAsync(id);

            if (diagnosticCode == null) return NotFound();

            return View(diagnosticCode);
        }

        /// <summary>
        /// Updates the <paramref name="diagnosticCode"/> in the database
        /// </summary>
        /// <param name="diagnosticCode">The diagnostic code to update</param>
        /// <returns>The details page of the code on a success, or the update form on a failure</returns>
        [HttpPost]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Update(DiagnosticCode diagnosticCode)
        {
            // Process the Markdown into HTML
            diagnosticCode.Html = _markdown.ToHtml(diagnosticCode.Markdown);

            // Validate the resulting DiagnosticCode 
            ModelState.ClearValidationState(nameof(DiagnosticCode));
            if (!TryValidateModel(diagnosticCode, nameof(DiagnosticCode)))
            {
                return View("Edit", diagnosticCode);
            }

            // Save the DiagnosticCode
            _context.DiagnosticCodes.Update(diagnosticCode);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", new { ID = diagnosticCode.ID });
        }

        #endregion
    }
}
