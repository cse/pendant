﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using KSU.CS.Pendant.Server.Models;

namespace KSU.CS.Pendant.Server.Controllers
{
    /// <summary>
    /// A controller for creating, viewing, updating, and deleting courses
    /// </summary>
    public class CoursesController : Controller
    {
        private readonly DataContext _context;

        /// <summary>
        /// Constructs a new CourseController using dependency injection
        /// </summary>
        /// <param name="context">The site database context</param>
        public CoursesController(DataContext context)
        {
            _context = context;
        }

        #region Route Methods

        /// <summary>
        /// Lists the available courses
        /// </summary>
        /// <returns>A HTML page listing the courses</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Index()
        {
            var courses = await _context.Courses.ToListAsync();

            return View(courses);
        }

        /// <summary>
        /// A page giving details of the selected course with <paramref name="id"/>
        /// </summary>
        /// <param name="id">The course database ID</param>
        /// <returns>A HTML page with the details</returns>
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Details(int id)
        {
            var course = await _context.Courses
                .Include(c => c.Assignments)
                .Include(c => c.IterativeProjects)
                .ThenInclude(p => p.Milestones)
                .Where(c => c.ID == id)
                .FirstOrDefaultAsync();

            if (course is null) return NotFound();

            return View(course);
        }

        /// <summary>
        /// Provides the form for creating a new course
        /// </summary>
        /// <returns>A HTML page containing the form</returns>
        [Route("/Courses/Create")]
        [Authorize(Policy = "AdminOnly")]
        public IActionResult Create()
        {
            var course = new Course();

            return View(course);
        }

        /// <summary>
        /// Adds a new course to the list of courses.  
        /// </summary>
        /// <returns>A redirect to the details page, or the form if corrections are needed</returns>
        [HttpPost]
        [Route("/Courses/Create")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> Create(Course course)
        {
            if (!ModelState.IsValid)
            {
                return View(course);
            }
                        
            // Save the course to the database
            _context.Courses.Add(course);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", "Courses", new { ID = course.ID });
        }

        #endregion
    }
}
