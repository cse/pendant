﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using KSU.CS.Pendant.Server.Models;
using KSU.CS.Pendant.Server.Helpers;
using Microsoft.EntityFrameworkCore;
using KSU.CS.Pendant.Server.Services;
using System.Text.Json;

namespace KSU.CS.Pendant.Server.Controllers
{
    /// <summary>
    /// A controller for creating and reading validations of user assignment submissions 
    /// </summary>
    public class ValidationsController : Controller
    {
        private readonly DataContext _context;

        /// <summary>
        /// Constructs a new ValidationsController using dependency injection
        /// </summary>
        /// <param name="context">The database context for the site</param>
        public ValidationsController(DataContext context)
        {
            _context = context;
        }

        #region Route Methods

        /// <summary>
        /// Lists all validations for the current user
        /// </summary>
        /// <returns>A HTML list of all validations</returns>
        public async Task<IActionResult> Index()
        {
            // Load the current user's data
            User user = await _context.Users
                .Include(u => u.ValidationAttempts.OrderByDescending(a => a.DateTime))
                .ThenInclude(va => va.Assignment)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            // Only show students thier own validation attempts 
            if (!(user.IsAdmin || user.IsFaculty || user.IsTeachingAssistant)) return View(user.ValidationAttempts);

            // load ALL validation attempts
            var attempts = await _context.ValidationAttempts
                .OrderByDescending(a => a.DateTime)
                .Include(a => a.User)
                .Include(a => a.Assignment)
                .ToListAsync();

            return View(attempts);
        }

        /// <summary>
        /// Displays the details of the specified validation attempt
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The details of the validation, or a 404 if it is not found or the user is not authorized</returns>
        public async Task<IActionResult> Details(int id, [FromQuery]string format)
        {
            var attempt = await _context.ValidationAttempts
                .Include(va => va.Assignment)
                .Include(va => va.User)
                .Where(va => va.ID == id)
                .FirstOrDefaultAsync();

            // Return a 404 if attempt is not found
            if (attempt is null) return StatusCode(404);

            // Load the current user's data
            User user = await _context.Users
                .Include(u => u.ValidationAttempts)
                .FirstAsync(i => i.ID == int.Parse(User.FindFirst("ID").Value));

            // Make sure the user is authorized to see this validation
            if (!(user.IsAdmin || user.IsFaculty || user.IsTeachingAssistant) && attempt.User.ID != int.Parse(User.FindFirst("ID").Value)) return StatusCode(404);

            // Return json if the user requests
            if (format == "json") return Json(new
            {
                design_issues = attempt.DesignIssues.Select(i => i.Message.ToString()),
                functional_issues = attempt.FunctionalIssues.Select(i => i.Message.ToString()),
                structural_issues = attempt.StructuralIssues.Select(i => i.Message.ToString()),
                style_issues = attempt.StyleIssues.Select(i => i.Message.ToString())
            }, new JsonSerializerOptions { WriteIndented = true,  Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping});

            return View(attempt);
        }

        /*
        /// <summary>
        /// Provides a form to create a new validation attempt
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Create()
        {
            ViewBag.Assignments = await _context.Assignments.ToListAsync();
            return View();
        }

        /// <summary>
        /// Create a new validation attempt for the specified assignment
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(int AssignmentID, string RepoURL, string Branch)
        {
            // Load the current user's data
            User user = await _context.Users.FindAsync(int.Parse(User.FindFirst("ID").Value));

            // Clone the repo
            var studentPath = await GithubHelper.CloneAndCheckout($"{user.EID}-{AssignmentID}", RepoURL, Branch);

            // Retrieve the assignment 
            var assignment = await _context.Assignments.FindAsync(AssignmentID);

            // Validate the assignment 
            var result = await ProgramVerifier.Verifier.Check(studentPath, assignment.SpecificationPath);

            // Create the ValidationAttempt
            var attempt = new ValidationAttempt()
            {
                User = user,
                Assignment = assignment,
                SolutionPath = studentPath,
                DateTime = DateTime.Now,
                Issues = result.Issues
            };

            _context.ValidationAttempts.Add(attempt);
            await _context.SaveChangesAsync();

            return RedirectToAction("Details", new { attempt.ID });
        }
        */

        #endregion
    }
}
