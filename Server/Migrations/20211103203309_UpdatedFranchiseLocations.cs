﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSU.CS.Pendant.Server.Migrations
{
    public partial class UpdatedFranchiseLocations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FranchiseLocations_Franchises_FranchiseID",
                table: "FranchiseLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_FranchiseLocations_User_UserID",
                table: "FranchiseLocations");

            migrationBuilder.DropTable(
                name: "Franchises");

            migrationBuilder.DropIndex(
                name: "IX_FranchiseLocations_FranchiseID",
                table: "FranchiseLocations");

            migrationBuilder.DropIndex(
                name: "IX_FranchiseLocations_UserID",
                table: "FranchiseLocations");

            migrationBuilder.DropColumn(
                name: "FranchiseID",
                table: "FranchiseLocations");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "FranchiseLocations");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "FranchiseLocations");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "FranchiseLocations",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FranchiseLocations_UserID",
                table: "FranchiseLocations",
                column: "UserID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_FranchiseLocations_User_UserID",
                table: "FranchiseLocations",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FranchiseLocations_User_UserID",
                table: "FranchiseLocations");

            migrationBuilder.DropIndex(
                name: "IX_FranchiseLocations_UserID",
                table: "FranchiseLocations");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "FranchiseLocations",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "FranchiseID",
                table: "FranchiseLocations",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "FranchiseLocations",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "FranchiseLocations",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FranchiseLocations_FranchiseID",
                table: "FranchiseLocations",
                column: "FranchiseID");

            migrationBuilder.CreateIndex(
                name: "IX_FranchiseLocations_UserID",
                table: "FranchiseLocations",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_FranchiseLocations_Franchises_FranchiseID",
                table: "FranchiseLocations",
                column: "FranchiseID",
                principalTable: "Franchises",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FranchiseLocations_User_UserID",
                table: "FranchiseLocations",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
