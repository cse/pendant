﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSU.CS.Pendant.Server.Migrations
{
    public partial class AddedIterativeProjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssignmentSeries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Courses",
                table: "Courses");

            migrationBuilder.DropColumn(
                name: "Course",
                table: "Assignment");

            migrationBuilder.RenameTable(
                name: "Courses",
                newName: "Course");

            migrationBuilder.AddColumn<int>(
                name: "AssignmentType",
                table: "Assignment",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CourseID",
                table: "Assignment",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IterativeProjectID",
                table: "Assignment",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Course",
                table: "Course",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "IterativeProjects",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CourseID = table.Column<int>(type: "int", nullable: false),
                    RepositoryName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AssignmentInvitationUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IterativeProjects", x => x.ID);
                    table.ForeignKey(
                        name: "FK_IterativeProjects_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Assignment_CourseID",
                table: "Assignment",
                column: "CourseID");

            migrationBuilder.CreateIndex(
                name: "IX_Assignment_IterativeProjectID",
                table: "Assignment",
                column: "IterativeProjectID");

            migrationBuilder.CreateIndex(
                name: "IX_IterativeProjects_CourseID",
                table: "IterativeProjects",
                column: "CourseID");

            migrationBuilder.AddForeignKey(
                name: "FK_Assignment_Course_CourseID",
                table: "Assignment",
                column: "CourseID",
                principalTable: "Course",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Assignment_IterativeProjects_IterativeProjectID",
                table: "Assignment",
                column: "IterativeProjectID",
                principalTable: "IterativeProjects",
                principalColumn: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignment_Course_CourseID",
                table: "Assignment");

            migrationBuilder.DropForeignKey(
                name: "FK_Assignment_IterativeProjects_IterativeProjectID",
                table: "Assignment");

            migrationBuilder.DropTable(
                name: "IterativeProjects");

            migrationBuilder.DropIndex(
                name: "IX_Assignment_CourseID",
                table: "Assignment");

            migrationBuilder.DropIndex(
                name: "IX_Assignment_IterativeProjectID",
                table: "Assignment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Course",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "AssignmentType",
                table: "Assignment");

            migrationBuilder.DropColumn(
                name: "CourseID",
                table: "Assignment");

            migrationBuilder.DropColumn(
                name: "IterativeProjectID",
                table: "Assignment");

            migrationBuilder.RenameTable(
                name: "Course",
                newName: "Courses");

            migrationBuilder.AddColumn<string>(
                name: "Course",
                table: "Assignment",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Courses",
                table: "Courses",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "AssignmentSeries",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AssignmentInvitationUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RepositoryName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignmentSeries", x => x.ID);
                });
        }
    }
}
