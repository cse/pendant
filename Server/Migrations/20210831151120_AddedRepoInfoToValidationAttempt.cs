﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSU.CS.Pendant.Server.Migrations
{
    public partial class AddedRepoInfoToValidationAttempt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GitHubAccount_User_UserId",
                table: "GitHubAccount");

            migrationBuilder.DropForeignKey(
                name: "FK_ValidationAttempt_Assignment_AssignmentID",
                table: "ValidationAttempt");

            migrationBuilder.DropForeignKey(
                name: "FK_ValidationAttempt_User_UserID",
                table: "ValidationAttempt");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "GitHubAccount",
                newName: "UserID");

            migrationBuilder.RenameIndex(
                name: "IX_GitHubAccount_UserId",
                table: "GitHubAccount",
                newName: "IX_GitHubAccount_UserID");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "ValidationAttempt",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AssignmentID",
                table: "ValidationAttempt",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CommitIdentifier",
                table: "ValidationAttempt",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RepoFullName",
                table: "ValidationAttempt",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_GitHubAccount_User_UserID",
                table: "GitHubAccount",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ValidationAttempt_Assignment_AssignmentID",
                table: "ValidationAttempt",
                column: "AssignmentID",
                principalTable: "Assignment",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ValidationAttempt_User_UserID",
                table: "ValidationAttempt",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GitHubAccount_User_UserID",
                table: "GitHubAccount");

            migrationBuilder.DropForeignKey(
                name: "FK_ValidationAttempt_Assignment_AssignmentID",
                table: "ValidationAttempt");

            migrationBuilder.DropForeignKey(
                name: "FK_ValidationAttempt_User_UserID",
                table: "ValidationAttempt");

            migrationBuilder.DropColumn(
                name: "CommitIdentifier",
                table: "ValidationAttempt");

            migrationBuilder.DropColumn(
                name: "RepoFullName",
                table: "ValidationAttempt");

            migrationBuilder.RenameColumn(
                name: "UserID",
                table: "GitHubAccount",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_GitHubAccount_UserID",
                table: "GitHubAccount",
                newName: "IX_GitHubAccount_UserId");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "ValidationAttempt",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "AssignmentID",
                table: "ValidationAttempt",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_GitHubAccount_User_UserId",
                table: "GitHubAccount",
                column: "UserId",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ValidationAttempt_Assignment_AssignmentID",
                table: "ValidationAttempt",
                column: "AssignmentID",
                principalTable: "Assignment",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ValidationAttempt_User_UserID",
                table: "ValidationAttempt",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
