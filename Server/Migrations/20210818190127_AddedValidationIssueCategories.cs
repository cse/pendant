﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSU.CS.Pendant.Server.Migrations
{
    public partial class AddedValidationIssueCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FunctionalIssues",
                table: "ValidationAttempt",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StructuralIssues",
                table: "ValidationAttempt",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StyleIssues",
                table: "ValidationAttempt",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FunctionalIssues",
                table: "ValidationAttempt");

            migrationBuilder.DropColumn(
                name: "StructuralIssues",
                table: "ValidationAttempt");

            migrationBuilder.DropColumn(
                name: "StyleIssues",
                table: "ValidationAttempt");
        }
    }
}
