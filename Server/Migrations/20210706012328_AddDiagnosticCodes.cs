﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KSU.CS.Pendant.Server.Migrations
{
    public partial class AddDiagnosticCodes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DiagnosticCode",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Markdown = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Html = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiagnosticCode", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DiagnosticCodeCheck",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<int>(type: "int", nullable: true),
                    DiagnosticCodeID = table.Column<int>(type: "int", nullable: true),
                    DateTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiagnosticCodeCheck", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DiagnosticCodeCheck_DiagnosticCode_DiagnosticCodeID",
                        column: x => x.DiagnosticCodeID,
                        principalTable: "DiagnosticCode",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DiagnosticCodeCheck_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DiagnosticCodeCheck_DiagnosticCodeID",
                table: "DiagnosticCodeCheck",
                column: "DiagnosticCodeID");

            migrationBuilder.CreateIndex(
                name: "IX_DiagnosticCodeCheck_UserID",
                table: "DiagnosticCodeCheck",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiagnosticCodeCheck");

            migrationBuilder.DropTable(
                name: "DiagnosticCode");
        }
    }
}
