﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSU.CS.Pendant.Server.Migrations
{
    public partial class AddedColumnsToIterativeProjectsAndAssignments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RepositoryName",
                table: "IterativeProjects",
                newName: "RepositoryPrefix");

            migrationBuilder.AlterColumn<string>(
                name: "AssignmentInvitationUrl",
                table: "IterativeProjects",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GitHubOrganizationName",
                table: "IterativeProjects",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "IterativeProjects",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DescriptionUrl",
                table: "Assignment",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GitHubOrganizationName",
                table: "IterativeProjects");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "IterativeProjects");

            migrationBuilder.DropColumn(
                name: "DescriptionUrl",
                table: "Assignment");

            migrationBuilder.RenameColumn(
                name: "RepositoryPrefix",
                table: "IterativeProjects",
                newName: "RepositoryName");

            migrationBuilder.AlterColumn<string>(
                name: "AssignmentInvitationUrl",
                table: "IterativeProjects",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }
    }
}
