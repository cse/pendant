﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSU.CS.Pendant.Server.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsStudent = table.Column<bool>(type: "bit", nullable: false),
                    IsFaculty = table.Column<bool>(type: "bit", nullable: false),
                    IsAdmin = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.ID);
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "ID", "EID", "FirstName", "IsAdmin", "IsFaculty", "IsStudent", "LastName" },
                values: new object[] { 1, "nhbean", "Nathan", false, false, false, "Bean" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "ID", "EID", "FirstName", "IsAdmin", "IsFaculty", "IsStudent", "LastName" },
                values: new object[] { 2, "russfeld", "Russ", false, false, false, "Feldhausen" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "ID", "EID", "FirstName", "IsAdmin", "IsFaculty", "IsStudent", "LastName" },
                values: new object[] { 3, "weeser", "Josh", false, false, false, "Weese" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
