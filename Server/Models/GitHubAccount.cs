﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class representing the connection to a student's GitHub account
    /// </summary>
    public class GitHubAccount
    {
        /// <summary>The database primary key</summary>
        public int ID { get; set; }

        /// <summary>Foriegn Key for User</summary>
        public int UserID { get; set; }

        /// <summary>The user this GitHub Account belongs to</summary>
        public User User { get; set; }

        /// <summary>The user's GitHub username</summary>
        public string Username { get; set; }

        /// <summary>A secret for securing webhooks</summary>
        public string Secret { get; set; } = Guid.NewGuid().ToString();

        /// <summary>A token for accessing GitHub on behalf of this account</summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Generates a new secret for this GitHub account
        /// An account will have only one secret at a time.
        /// </summary>
        public void GenerateSecret()
        {
            this.Secret = Guid.NewGuid().ToString();
        }
    }
}
