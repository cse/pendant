﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    /// <summary>
    /// A class connecting assignments to an iterative project
    /// </summary>
    public class MilestoneAssignment : Assignment
    {

        /// <summary>
        /// The assignment series this milestone belongs to
        /// </summary>
        public IterativeProject IterativeProject { get; set; }

        /// <summary>
        /// The foreign key to the assigment series
        /// </summary>
        public int IterativeProjectID { get; set; }
                
    }
}
