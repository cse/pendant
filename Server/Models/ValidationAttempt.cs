﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    public enum ValidationAttemptStatus
    {
        Pending,
        Complete,

    }
    /// <summary>
    /// A class representing an attempt by a student 
    /// to validate thier assignment solution
    /// </summary>
    public class ValidationAttempt
    {
        /// <summary>The database primary key</summary>
        public int ID { get; set; }

        /// <summary>
        /// The user who made the attempt
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// The database foreign key to the user
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// The assignment this attempt is for
        /// </summary>
        public Assignment Assignment { get; set; }

        /// <summary>
        /// The database foriegn key to the assignment
        /// </summary>
        public int AssignmentID { get; set; }

        /// <summary>
        /// The date and time the attempt was made
        /// </summary>
        public DateTime DateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// The total number of issues encountered in the attempt
        /// </summary>
        public int IssueCount => StructuralIssues.Count + FunctionalIssues.Count + DesignIssues.Count + StyleIssues.Count;

        /// <summary>
        /// The structural issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> StructuralIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The functional issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> FunctionalIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The design issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> DesignIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The style issues encountered in the attempt
        /// </summary>
        public List<ValidationIssue> StyleIssues { get; set; } = new List<ValidationIssue>();

        /// <summary>
        /// The issues (style and design) determined by code analysis
        /// </summary>
        public List<ValidationIssue> DiagnosticIssues
        {
            get
            {
                List<ValidationIssue> issues = new();
                issues.AddRange(this.DesignIssues);
                issues.AddRange(this.StyleIssues);
                return issues;
            }
        }

        /// <summary>
        /// The location of the student's work in the file system
        /// </summary>
        public string SolutionPath { get; set; }

        /// <summary>
        /// The student repository's full name
        /// </summary>
        public string RepoFullName { get; set; }

        /// <summary>
        /// The student repository's commit identifier
        /// </summary>
        public string CommitIdentifier { get; set; }

        /// <summary>
        /// A direct link to the validated commit in the GitHub repo, or null if one is not available
        /// </summary>
        public string GitHubUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CommitIdentifier) || string.IsNullOrWhiteSpace(RepoFullName)) return null;
                else return $"https://github.com/{RepoFullName}/tree/{CommitIdentifier}";
            }
        }

        /// <summary>
        /// A unique identifier for the validation attempt
        /// </summary>
        public string Identity => $"{RepoFullName} - {CommitIdentifier} - {DateTime}";

    }
}
