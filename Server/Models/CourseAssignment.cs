﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSU.CS.Pendant.Server.Models
{
    public class CourseAssignment : Assignment
    {
        /// <summary>
        /// The course this assignment belongs to
        /// </summary>
        public Course Course { get; set; }

        /// <summary>
        /// The foreign key to the course
        /// </summary>
        //[Required]
        public int CourseID { get; set; }
    }
}
