﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace KSU.CS.Pendant.Server.Models
{
    public enum AssignmentType
    {
        Assignment,
        CourseAssignment,
        MilestoneAssignment
    }

    public class Assignment
    {
        /// <summary>
        /// The database primary key
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The discriminator column for entity framework inheritance
        /// </summary>
       public AssignmentType AssignmentType { get; set; }

        /// <summary>The assignment name</summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The path to the specification folder for the assignment
        /// </summary>
        public string SpecificationPath { get; set; }

        /// <summary>
        /// The branch name of the repository to evaluate for this assignment
        /// (used with webhooks)
        /// </summary>
        [Required]
        public string BranchName { get; set; } = "main";

        /// <summary>
        /// The url to the description of the assignment
        /// </summary>
        [Required]
        public string DescriptionUrl { get; set; }

        /// <summary>Specification archive for the assignment</summary>
        [Required]
        [DataType(DataType.Upload)]
        [NotMapped]
        public IFormFile Specification { get; set; }

        /// <summary>
        /// The attempts by students to validate thier solutions for this assignment
        /// </summary>
        public ICollection<ValidationAttempt> ValidationAttempts { get; set; }

    }
}
