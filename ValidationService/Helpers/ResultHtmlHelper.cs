﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSU.CS.ProgramVerifier;

namespace KSU.CS.Pendant.ValidationService.Helpers
{
    public static class ResultHtmlHelper
    {
        public static string RenderToHtml(AnalysisResult result, string gitHubUrl)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<h5>Structural Issues</h5>");
            sb.AppendLine("<p>Structural issues mean your code does not match the structure laid out in the assignment specification.You may need to re-read the assignment specification carefully for this and prior milestones to make sure you are building your program to match the specification.</p>");
            if (result.StructuralIssues.Count == 0) AppendNoIssuesFound(sb, "structural");
            else AppendIssuesFound(sb, "structural", result.StructuralIssues);

            sb.AppendLine("<h5>Design Issues</h5>");
            sb.AppendLine("<p>Design issues concern your use of C# and its libraries.  Are they used appropriately?</p>");
            if (result.DesignIssues.Count == 0) AppendNoIssuesFound(sb, "design");
            else AppendIssuesFound(sb, "design", gitHubUrl, result.DesignIssues);

            sb.AppendLine("<h5>Functional Issues</h5>");
            sb.AppendLine("<p>Functional issues concern the actual operation of your program - i.e. does it do what was asked in the assignment specification?</p>");
            if (!result.DoesCompile) AppendNoIssuesChecked(sb, "functional");
            else if (result.FunctionalIssues.Count == 0) AppendNoIssuesFound(sb, "functional");
            else AppendIssuesFound(sb, "functional", result.FunctionalIssues);

            sb.AppendLine("<h5>Style Issues</h5>");
            sb.AppendLine("<p>Style issues concern how your code is written - is it legibile, well-documented, and does it follow the coding practices adopted by the CS department at K-State?</p>");
            if (result.StyleIssues.Count == 0) AppendNoIssuesFound(sb, "style");
            else AppendIssuesFound(sb, "style", gitHubUrl, result.StyleIssues);

            return sb.ToString();
        }

        private static void AppendNoIssuesChecked(StringBuilder sb, string category)
        {
            sb.AppendLine("<p class=\"alert alert-warning\">");
            sb.AppendLine("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-exclamation-octagon-fill\" viewBox=\"0 0 16 16\"><path d=\"M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z\" /><path d=\"M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z\"/></svg>");
            sb.AppendLine($"Any {category} issues cannot be checked for until structural and design issues are fixed.");
            sb.AppendLine("</p>");
        }

        private static void AppendNoIssuesFound(StringBuilder sb, string category)
        {
            sb.AppendLine("<p class=\"alert alert-success\">");
            sb.AppendLine("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-exclamation-octagon-fill\" viewBox=\"0 0 16 16\"><path d=\"M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z\" /></svg>");
            sb.AppendLine($"No {category} issues were found in your program!");
            sb.AppendLine("</p>");
        }

        private static void AppendIssuesFound(StringBuilder sb, string category, List<string> issues)
        {
            sb.AppendLine("<div class=\"card-header bg-danger text-white container\">");
            sb.AppendLine("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-exclamation-octagon-fill\" viewBox=\"0 0 16 16\"><path d=\"M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zM8 4c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995A.905.905 0 0 1 8 4zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z\" /></svg>");
            sb.AppendLine($"The following {category} issues were found in your program and will need to be corrected:");
            sb.AppendLine("</div>");
            sb.AppendLine("<ul class=\"list-group list-group-flush\">");
            foreach (var issue in issues)
            {
                sb.Append("<li class=\"list-group-item\">");
                sb.Append(issue);
                sb.AppendLine("</li>");
            }
        }

        private static void AppendIssuesFound(StringBuilder sb, string category, string gitHubUrl, List<AnalysisIssue> issues)
        {
            sb.AppendLine("<div class=\"card-header bg-danger text-white container\">");
            sb.AppendLine("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-exclamation-octagon-fill\" viewBox=\"0 0 16 16\"><path d=\"M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zM8 4c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995A.905.905 0 0 1 8 4zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z\" /></svg>");
            sb.AppendLine($"The following {category} issues were found in your program and will need to be corrected:");
            sb.AppendLine("</div>");
            sb.AppendLine("<ul class=\"list-group list-group-flush\">");
            foreach (var issue in issues)
            {
                sb.AppendLine("<li class=\"list-group-item\">");
                sb.AppendLine(issue.Message);
                if(!String.IsNullOrEmpty(issue.HelpUrl))
                {
                    sb.AppendLine($"<a href=\"{issue.HelpUrl}\" class=\"text-danger px-2\">Get Help</a>");
                }
                if(!String.IsNullOrEmpty(issue.Path))
                {
                    string sourceUrl = gitHubUrl + issue.Path;
                    sb.AppendLine($"<a href=\"{sourceUrl}\" class=\"text-success px-2\">Show Location</a>");
                }
                sb.AppendLine("</li>");
            }
        }


    }
}
