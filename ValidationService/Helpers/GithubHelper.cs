﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace KSU.CS.Pendant.ValidationService.Helpers
{
    public class GithubHelper
    {
        public static async Task<string> CloneAndCheckout(string repoUrl, string commitID)
        {
            var path = "C:\\users\\nhbean\\Desktop\\trial";// System.IO.Path.GetTempPath();

            var psi = new ProcessStartInfo() {
                FileName = "cmd.exe",
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            using var process = Process.Start(psi);

            using StreamWriter writer = process.StandardInput;
            
            if (writer.BaseStream.CanWrite)
            {
                // Clone the repo (if it doesn't already exist 
                writer.WriteLine($"git clone {repoUrl} {path}");
                // Change into the repo directory
                writer.WriteLine($"cd {path}");
                // Checkout commit 
                writer.WriteLine($"git checkout {commitID}");


                // Create a remote tracking branch (if it doesn't already exist)
                //writer.WriteLine($"git checkout -b {branch} origin/{branch}");
                // Check out the branch (we must do this if the branch already existed)
                //writer.WriteLine($"git checkout {branch}");
                // 
                writer.WriteLine("exit");
            }
            
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            await process.WaitForExitAsync();
            return path;
        }
    }
}
