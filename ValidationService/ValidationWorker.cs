﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KSU.CS.Pendant.ValidationService.Helpers;
using KSU.CS.ProgramVerifier;
using KSU.CS.Pendant.Server;
using Microsoft.EntityFrameworkCore;

namespace KSU.CS.Pendant.ValidationService
{
    public class ValidationWorker : BackgroundService
    {
        private readonly DataContext _context;
        private readonly ILogger<ValidationWorker> _logger;

        public ValidationWorker(ILogger<ValidationWorker> logger, DataContext context)
        {
            _logger = logger;
            _context = context;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            //var tcs = new TaskCompletionSource<bool>();
            //cancellationToken.Register(s => ((TaskCompletionSource<bool>)s).SetResult(true), tcs);
            //await tcs.Task;

            while (!cancellationToken.IsCancellationRequested)
            {

                var attempt = await _context.ValidationAttempts
                    .Include(a => a.Assignment)
                    .Where(a => !a.IsChecked)
                    .FirstOrDefaultAsync();

                if (attempt != null)
                {

                    _logger.LogWarning($"Cloning {attempt.RepoUrl}");

                    var path = await GithubHelper.CloneAndCheckout(attempt.RepoUrl, attempt.CommitIdentifier);

                    _logger.LogWarning($"Successfully cloned to {path}, starting verification...");

                    var results = await Verifier.Check(path, attempt.Assignment.SpecificationPath);

                    _logger.LogWarning("Results obtained, rendering to HTML");

                    var html = ResultHtmlHelper.RenderToHtml(results, attempt);

                    _logger.LogWarning("Saving to the database");

                    attempt.Report = html;
                    attempt.IsChecked = true;

                    _context.Update(attempt);
                    await _context.SaveChangesAsync();
                }

                    /*
                    var validationRequest = new ValidationRequest()
                    {
                        RepoUrl = "git@github.com:ksu-cis/webhook-test-zombiepaladin.git",
                        CommitID = "276dfb9315b93b525e0dd3f57eb5c58cf4f9bae8",
                        SpecificationPath = "C:\\workDirectory\\specifications\\v3mdy40k.xak\\ms1"
                    };

                    _logger.LogInformation($"Cloning {validationRequest.RepoUrl}");

                    var path = GithubHelper.CloneAndCheckout(validationRequest.RepoUrl, validationRequest.CommitID).GetAwaiter().GetResult();

                    _logger.LogInformation($"Successfully cloned to {path}, starting verification...");

                    var results = Verifier.Check(path, validationRequest.SpecificationPath).GetAwaiter().GetResult();

                    _logger.LogInformation("Results obtained, rendering to HTML");

                    var html = ResultHtmlHelper.RenderToHtml(results, validationRequest);

                    _logger.LogInformation("HTML Rendered, writing to file...");

                    await System.IO.File.WriteAllTextAsync("C:\\workDirectory\\results.html", html, cancellationToken);

                    _logger.LogInformation("File saved, waiting for next go-round");
                    */

                await Task.Delay(TimeSpan.FromSeconds(10), cancellationToken);

            }

            _logger.LogInformation("Service stopped");
        }
    }
}
