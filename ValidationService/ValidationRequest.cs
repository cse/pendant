﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService
{
    public class ValidationRequest
    {
        public string RepoURL { get; set; }

        public string CommitID { get; set; }

        public string SpecificationPath { get; set; }
    }
}
