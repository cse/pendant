﻿using System;
using System.IO;
using KSU.CS.Pendant.ValidationService.Helpers;
using KSU.CS.ProgramVerifier;

namespace WindowsService
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var validationRequest = new ValidationRequest()
            {
                RepoURL = "git@github.com:ksu-cis/webhook-test-zombiepaladin.git",
                CommitID = "276dfb9315b93b525e0dd3f57eb5c58cf4f9bae8",
                SpecificationPath = "C:\\workDirectory\\specifications\\v3mdy40k.xak\\ms1"
            };

            var path = GithubHelper.CloneAndCheckout(validationRequest.RepoURL, validationRequest.CommitID).GetAwaiter().GetResult();

            Console.WriteLine("Done, moving on...");
            Console.WriteLine(path);

            var results = Verifier.Check(path, validationRequest.SpecificationPath).GetAwaiter().GetResult();

            var html = ResultHtmlHelper.RenderToHtml(results, validationRequest.RepoURL);
            Console.WriteLine(html);
            
        }
    }
}
