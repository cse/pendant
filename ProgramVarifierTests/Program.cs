﻿using System;
using KSU.CS.ProgramVerifier;

namespace ProgramVarifierTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let's get ready to Verify!");
            var specificationPath = @"C:\Users\nhbean\Teaching\CIS400\Specifications\ms3";
            var solutionPath = @"C:\Users\nhbean\Teaching\CIS400\GyroScope";

            var result = Verifier.Check(solutionPath, specificationPath).GetAwaiter().GetResult();
            foreach(var issue in result.StructuralIssues)
            {
                Console.WriteLine(issue);
            }
        }
    }
}
