﻿using System.Diagnostics;
using Microsoft.CodeAnalysis;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSU.CS.ProgramVerifier
{
    public static class NugetManager
    {
        public static async Task Restore(Solution solution)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = "cmd.exe";
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;

            using (var process = Process.Start(psi))
            {
                using (StreamWriter writer = process.StandardInput)
                {
                    if (writer.BaseStream.CanWrite)
                    {
                        writer.WriteLine($"cd ${solution.FilePath}");
                        writer.WriteLine($"nuget update");
                        writer.WriteLine("exit");
                    }
                }
                await process.WaitForExitAsync();
            }
        }
    }
}
