﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSU.CS.ProgramVerifier
{

    public class AnalysisIssue
    {
        public string Message { get; set; }

        public string Path { get; set; }

        public string Line { get; set; }

        public string HelpUrl { get; set; }
    }


    public class AnalysisResult
    {
        public bool DoesCompile { get; set; }

        public List<string> Issues
        {
            get 
            {
                var issues = new List<string>();
                issues.AddRange(StructuralIssues);
                issues.AddRange(FunctionalIssues);
                issues.AddRange(DesignIssues.Select(s => s.Message));
                issues.AddRange(StyleIssues.Select(s => s.Message));
                return issues;
            }
        }

        public List<string> StructuralIssues { get; set; } = new List<string>();

        public List<string> FunctionalIssues { get; set; } = new List<string>();

        public List<AnalysisIssue> DesignIssues { get; set; } = new List<AnalysisIssue>();

        public List<AnalysisIssue> StyleIssues { get; set; } = new List<AnalysisIssue>();

        public int DiscoveredTests { get; set; }

        public int SkippedTests { get; set; }

        public int FailedTests { get; set; }


        public AnalysisResult()
        {
            DoesCompile = true;
        }
    }
}
