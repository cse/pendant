﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Build;
using System.IO;
using System.Reflection;
using Xunit.Runners;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Xml.Linq;

namespace KSU.CS.ProgramVerifier
{
    public static class TestRunner
    {
        public static async Task Run(Solution solution, AnalysisResult result)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = "cmd.exe";
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            using (var process = Process.Start(psi))
            {
                using (StreamWriter writer = process.StandardInput)
                {
                    if (writer.BaseStream.CanWrite)
                    {
                        writer.WriteLine($"cd ${solution.FilePath}");
                        writer.WriteLine($"dotnet test --logger \"trx;LogFileName=TestResults.trx\"");
                    }
                }
                await process.WaitForExitAsync();

                Console.WriteLine(await process.StandardOutput.ReadToEndAsync());
                Console.WriteLine(await process.StandardError.ReadToEndAsync());
            }

            foreach(var project in solution.Projects)
            {
                var path = Path.Join(project.FilePath, "..", "TestResults", "TestResults.trx");
                Console.WriteLine("Attempting to read " + path);
                if (File.Exists(path))
                {
                    var testResults = XElement.Load(path);
                    var errors = testResults.Descendants("ErrorInfo").ToList();
                    foreach (var errorInfo in errors)
                    {
                        var message = errorInfo.Descendants("Message").First().Value;
                        result.FunctionalIssues.Add(message);
                    }
                }
            }
            
        }
    }
}
