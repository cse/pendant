﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSU.CS.ProgramVerfiier
{
    /// <summary>
    /// A class representing a student's project
    /// </summary>
    public class StudentProject
    {
        /// <summary>
        /// The path to the student's project
        /// </summary>
        public string Path { get; protected set; }

    }
}
