﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Build;
using System.IO;
using System.Reflection;
using Xunit.Runners;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace KSU.CS.ProgramVerifier
{
    public static class SpecificationTestRunner
    {
        public static async Task Run(Specification specification, Workspace workspace, AnalysisResult result)
        {
            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", "Starting tests...\n");
            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"Specification directory {specification.Path}\n");
            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"XUnit tests: {specification.HasXUnitTests}\n");
            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"NUnit tests: {specification.HasNUnitTests}\n");
            
            var solutionFileName = Path.GetFileName(workspace.CurrentSolution.FilePath);
            var solutionPath = Path.GetDirectoryName(workspace.CurrentSolution.FilePath);
            var studentPath = Path.GetDirectoryName(workspace.CurrentSolution.FilePath);
            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"Student directory {studentPath}\n");

            var psi = new ProcessStartInfo();
            psi.FileName = "cmd.exe";
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            var password = new System.Security.SecureString();
            foreach (char c in "insecurepassword123") password.AppendChar(c);
            psi.UserName = "nhbean_local";
#pragma warning disable CA1416 // Validate platform compatibility
            psi.Password = password;
#pragma warning restore CA1416 // Validate platform compatibility


            using (var process = Process.Start(psi))
            {
                using (StreamWriter writer = process.StandardInput)
                {
                    if (writer.BaseStream.CanWrite)
                    {
                        writer.WriteLine($"cd {studentPath}");

                        if (specification.HasXUnitTests)
                        {
                            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\ndotnet new xunit -o XUnitValidations");
                            writer.WriteLine($"dotnet new xunit -o XUnitValidations");
                            
                            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\nXcopy {specification.XUnitPath} {studentPath}\\XUnitValidations /s /i /y");
                            writer.WriteLine($"Xcopy {specification.XUnitPath} {studentPath}\\XUnitValidations /s /i /y");
                            
                            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\ndotnet sln {solutionFileName} add XUnitValidations");
                            writer.WriteLine($"dotnet sln {solutionFileName} add XUnitValidations");
                            
                            foreach (var project in workspace.CurrentSolution.Projects)
                            {
                                // Find the relative project path
                                var projectPath = project.FilePath[(solutionPath.Length + 1)..];
                                await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\ndotnet add XUnitValidations/XUnitValidations.csproj reference {projectPath}");
                                writer.WriteLine($"dotnet add XUnitValidations/XUnitValidations.csproj reference {projectPath}");
                            }
                            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\ndotnet test XUnitValidations --logger \"trx;LogFileName=XUnitResults.trx\"");
                            writer.WriteLine($"dotnet test XUnitValidations --logger \"trx;LogFileName=TestResults.trx\"");
                        }
                        writer.WriteLine("exit");
                    }
                }
                await process.WaitForExitAsync();

                await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", await process.StandardOutput.ReadToEndAsync());
                await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", await process.StandardError.ReadToEndAsync());

                
                var xUnitPath = Path.Join(studentPath, "XUnitValidations\\TestResults\\TestResults.trx");
                await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\n\n Looking for test results in {xUnitPath}");
                if (File.Exists(xUnitPath))
                {
                    var ns = "{http://microsoft.com/schemas/VisualStudio/TeamTest/2010}";
                    var testResults = XElement.Load(xUnitPath);
                    var summary = testResults.Descendants($"{ns}ResultsSummary").First();
                    await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\n\n Validation Tests {summary.Attribute("outcome").Value}");
                    if (summary.Attribute("outcome").Value == "Failed")
                    {
                        result.FunctionalIssues.Add("Unable to complete Functional Tests, most likely due to the test process crashing.");
                        result.FunctionalIssues.Add("This is often caused by errors in your program logic, i.e. setting up an infinite loop or recursion.");
                        var text = testResults.Descendants($"{ns}Text").First();
                        if (text != null) result.FunctionalIssues.Add("TestRunner reported: " + text.Value);
                    }
                    else
                    {
                        var errors = testResults.Descendants($"{ns}ErrorInfo").ToList();
                        await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\n\n Errors: {errors.Count}");
                        foreach (var errorInfo in errors)
                        {
                            var message = errorInfo.Descendants($"{ns}Message").First().Value;
                            message = Regex.Replace(message, "\\nExpected:\\s+(True|False)", "");
                            message = Regex.Replace(message, "\\nActual:\\s+(True|False)", "");
                            await File.AppendAllTextAsync(@"C:\workDirectory\log.txt", $"\n\n Message: {message}");
                            result.FunctionalIssues.Add(message);
                        }
                    }
                }
            }
        }
    }
}
