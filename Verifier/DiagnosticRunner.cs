﻿using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using KSU.CS.Pendant.CodeAnalysis.DesignAnalyzers;
using KSU.CS.Pendant.CodeAnalysis.StyleAnalyzers;

namespace KSU.CS.ProgramVerifier
{
    public static class DiagnosticRunner
    {
        private static readonly ImmutableArray<DiagnosticAnalyzer> _designAnalyzers;
        private static readonly ImmutableArray<DiagnosticAnalyzer> _styleAnalyzers;

        static DiagnosticRunner()
        {
            _designAnalyzers = new DiagnosticAnalyzer[]
            {
                new PropertySelfReferenceAnalyzer(),
                new NewInMemberAnalyzer()
            }.ToImmutableArray();

            _styleAnalyzers = new DiagnosticAnalyzer[]
            {
                new NamingConventionsAnalyzer(),
                new CommentsAnalyzer()
            }.ToImmutableArray();
        }

        public static async Task Run(Solution solution, AnalysisResult result)
        {
            // Determine the starting point for relative file paths in the solution
            int pathStartIndex = System.IO.Path.GetDirectoryName(solution.FilePath).Length;
            // Run diagnostics on all projects in the solution
            foreach(Project project in solution.Projects)
            {
                // Create a base compilation
                var compilation = await project.GetCompilationAsync();
                foreach (var diagnostic in compilation.GetDiagnostics())
                {
                    // We currenlty don't report the issues reported by the default analyzers
                    //result.Issues.Add(diagnostic.GetMessage());
                    
                    // Errors should prevent full compilation
                    if (diagnostic.Severity == DiagnosticSeverity.Error) result.DoesCompile = false;
                }

                // Create a compilation focused on design issues
                var compilationWithDesignAnalyzers = compilation.WithAnalyzers(_designAnalyzers);
                foreach (var diagnostic in await compilationWithDesignAnalyzers.GetAnalyzerDiagnosticsAsync())
                {
                    var linespan = diagnostic.Location.GetLineSpan();
                    result.DesignIssues.Add(new AnalysisIssue()
                    {
                        Message = diagnostic.GetMessage(),
                        Path = linespan.IsValid ? linespan.Path[pathStartIndex..] : "",
                        Line = linespan.IsValid ? linespan.StartLinePosition.Line.ToString() : "",
                        HelpUrl = $"https://pendant.cs.ksu.edu/Diagnostic/{diagnostic.Id}"
                    }); 
                    // Errors should prevent full compilation
                    if (diagnostic.Severity == DiagnosticSeverity.Error) result.DoesCompile = false;
                }

                // Create a compilation focused on style issues
                var compilationWithStyleAnalyzers = compilation.WithAnalyzers(_styleAnalyzers);
                foreach (var diagnostic in await compilationWithStyleAnalyzers.GetAnalyzerDiagnosticsAsync())
                {
                    var linespan = diagnostic.Location.GetLineSpan();

                    result.StyleIssues.Add(new AnalysisIssue()
                    {
                        Message = diagnostic.GetMessage(),
                        Path = linespan.IsValid ? linespan.Path[pathStartIndex..] : "",
                        Line = linespan.IsValid ? linespan.StartLinePosition.Line.ToString() : "",
                        HelpUrl = $"https://pendant.cs.ksu.edu/Diagnostic/{diagnostic.Id}"
                    });
                    // Errors should prevent full compilation
                    if (diagnostic.Severity == DiagnosticSeverity.Error) result.DoesCompile = false;
                }
            }            
        }
    }
}
