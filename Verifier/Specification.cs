﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UMLSpecification;

namespace KSU.CS.ProgramVerifier
{
    /// <summary>
    /// A class representing a solution specification
    /// </summary>
    /// <remarks>
    /// A solution specification is 
    /// a directory containing a `structure.json` file describing the structure of 
    /// the C# project(s) needed to complete an assignment.
    /// </remarks>
    public class Specification
    {
        /// <summary>
        /// Loads a project specification asynchronously.  
        /// </summary>
        /// <param name="path">The path to the solution specification directory</param>
        /// <returns>The loaded project specification</returns>
        public static async Task<Specification> LoadAsync(string path)
        {
            var json = await File.ReadAllTextAsync($"{path}/structure.json");

            var structure = JsonConvert.DeserializeObject<SolutionDefinition>(json);

            return new Specification()
            {
                Path = path,
                Structure = structure
            };
        }

        /// <summary>
        /// The path to the project specification files
        /// </summary>
        public string Path { get; init; }

        public bool HasXUnitTests => System.IO.Directory.Exists(XUnitPath);

        public string XUnitPath => System.IO.Path.Join(Path, "XUnit");

        public bool HasNUnitTests => System.IO.Directory.Exists(NUnitPath);

        public string NUnitPath => System.IO.Path.Join(Path, "NUnit");

        /// <summary>
        /// The solution structure
        /// </summary>
        public SolutionDefinition Structure { get; init; }

    }
}
