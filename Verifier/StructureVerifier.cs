﻿using System;
using System.Text.Json;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Build.Locator;
using Newtonsoft.Json;
using UMLSpecification;

namespace KSU.CS.ProgramVerifier
{
    public static class StructureVerifier
    {
        /// <summary>
        /// Verify the structure of a student project matches the specification
        /// </summary>
        /// <param name="projectDef">The specification the student is attempting to match</param>
        /// <param name="project">The student's project</param>
        /// <param name="result">An anaysis result object we register found issues with</param>
        /// <returns></returns>
        public static async Task CheckProjectStructure(ProjectDefinition projectDef, Project project, AnalysisResult result)
        {
            // Compile project (needed for the semantic structural analysis)
            var compilation = await project.GetCompilationAsync();

            // Check enumerations 
            foreach (var enumeration in projectDef.Enums)
            {
                var enumerationDoc = project.Documents.Where(document => document.Name.Equals(enumeration.Name + ".cs")).FirstOrDefault();
                if (enumerationDoc == null)
                {
                    result.StructuralIssues.Add($"Expected the enum {enumeration.Name} to be defined in a file named {enumeration.Name}.cs");
                }
                else
                {
                    var tree = await enumerationDoc.GetSyntaxTreeAsync();
                    var model = compilation.GetSemanticModel(tree);
                    var enumSyntax = tree.GetRoot().DescendantNodes()
                        .OfType<EnumDeclarationSyntax>()
                        .Where(eds => eds.Identifier.ValueText.Equals(enumeration.Name))
                        .FirstOrDefault();
                    CheckEnumerationStructure(model, enumeration, enumSyntax, result.StructuralIssues);
                }
            }

            // Check interfaces
            foreach (var interfaceDef in projectDef.Interfaces)
            {
                var interfaceDoc = project.Documents.Where(document => document.Name.Equals(interfaceDef.Name + ".cs")).FirstOrDefault();
                if (interfaceDoc == null)
                {
                    result.StructuralIssues.Add($"Expected the interface {interfaceDef.Name} to be defined in a file named {interfaceDef.Name}.cs");
                }
                else
                {
                    var tree = await interfaceDoc.GetSyntaxTreeAsync();
                    var model = compilation.GetSemanticModel(tree);
                    var interfaceSyntax = tree.GetRoot().DescendantNodes()
                        .OfType<InterfaceDeclarationSyntax>()
                        .Where(eds => eds.Identifier.ValueText.Equals(interfaceDef.Name))
                        .FirstOrDefault();
                    CheckInterfaceStructure(model, interfaceDef, interfaceSyntax, result.StructuralIssues);
                }
            }

            // Check classes
            foreach (var classDef in projectDef.Classes)
            {
                var classDoc = project.Documents.Where(document => document.Name.Equals(classDef.Name + ".cs")).FirstOrDefault();
                if (classDoc == null)
                {
                    result.StructuralIssues.Add($"Expected the class {classDef.Name} to be defined in a file named {classDef.Name}.cs");
                }
                else
                {
                    var tree = await classDoc.GetSyntaxTreeAsync();
                    var model = compilation.GetSemanticModel(tree);
                    var classSyntax = tree.GetRoot().DescendantNodes() 
                        .OfType<ClassDeclarationSyntax>()
                        .Where(eds => eds.Identifier.ValueText.Equals(classDef.Name))
                        .FirstOrDefault();
                    CheckClassStructure(model, classDef, classSyntax, result.StructuralIssues);
                }
            }
        }

        /// <summary>
        /// Checks the supplied <paramref name="enumSyntax"/> against the <paramref name="enumerationDef"/> specification, and 
        /// logs any issues to <paramref name="issues"/>
        /// </summary>
        /// <param name="enumerationDef">The enum specification</param>
        /// <param name="enumSyntax">The actual enum syntax</param>
        /// <param name="issues">A list of issues found in the project</param>
        static void CheckEnumerationStructure(SemanticModel model, UMLSpecification.EnumDefinition enumerationDef, EnumDeclarationSyntax enumSyntax, List<string> issues)
        {
            if (enumSyntax == null)
            {
                issues.Add($"Missing enum {enumerationDef.Name}.");
            }
            else
            {
                if (!VisibilityCheck(enumerationDef.Visibility, enumSyntax))
                {
                    issues.Add($"Expected enum {enumerationDef.Name} to be declared {enumerationDef.Visibility.ToString().ToLower()}");
                }
                foreach (var member in enumerationDef.Members)
                {
                    var enumMemberSyntax = enumSyntax.DescendantNodes()
                            .OfType<EnumMemberDeclarationSyntax>()
                            .Where(emds => emds.Identifier.ValueText.Equals(member.Name))
                            .FirstOrDefault();
                    if (enumMemberSyntax == null)
                        issues.Add($"Missing enum {enumerationDef.Name} member {member.Name}.");
                    else if (member.Value != null && (enumMemberSyntax.EqualsValue == null || enumMemberSyntax.EqualsValue.Value.ToString() != member.Value))
                    {
                        issues.Add($"Expected enum {enumerationDef.Name} member {member.Name} to have specified value {member.Value}");
                    }
                }
            }
        }

        static void CheckInterfaceStructure(SemanticModel model, UMLSpecification.InterfaceDefinition interfaceDef, InterfaceDeclarationSyntax interfaceSyntax, List<string> issues)
        {
            if (interfaceSyntax == null)
            {
                issues.Add($"Missing interface {interfaceDef.Name}.");
            }
            else
            {
                // Check interface visibility
                if (!VisibilityCheck(interfaceDef.Visibility, interfaceSyntax))
                {
                    issues.Add($"Expected interface {interfaceDef.Name} to be declared {interfaceDef.Visibility.ToString().ToLower()}");
                }

                // Check interface events 

                // Check interface properties
                PropertyCheck(model, interfaceDef, interfaceSyntax, issues);

                // Check interface methods
                MethodCheck(interfaceDef, interfaceSyntax, issues);


                // Check interface indexers
            }
        }

        static void CheckClassStructure(SemanticModel model, UMLSpecification.ClassDefinition classDef, ClassDeclarationSyntax classSyntax, List<string> issues)
        {
            if (classSyntax == null)
            {
                issues.Add($"Missing class {classDef.Name}.");
            }
            else
            {
                // Check visibility
                if (!VisibilityCheck(classDef.Visibility, classSyntax))
                {
                    issues.Add($"Expected class {classDef.Name} to be declared {classDef.Visibility.ToString().ToLower()}");
                }

                // Check abstract status 
                if(classDef.Abstract && !AbstractCheck(classSyntax))
                {
                    issues.Add($"Expected class {classDef.Name} to be declared abstract");
                }

                // Check base class 
                if(classDef.BaseClass != null)
                {
                    var symbol = model.GetDeclaredSymbol(classSyntax).BaseType;
                    if(symbol == null || symbol.ToMinimalDisplayString(model, classSyntax.SpanStart) != classDef.BaseClass)
                    {
                        issues.Add($"Expected class {classDef.Name} to inherit from class {classDef.BaseClass}");
                    }
                }

                // Check interfaces

                // Check class events 

                // Check class fields

                // Check class properties
                PropertyCheck(model, classDef, classSyntax, issues);

                // Check class methods
                MethodCheck(classDef, classSyntax, issues);

                // Check interface indexer
            }
        }

        /// <summary>
        /// Checks that the properties defined in <paramref name="syntax"/> match
        /// those specified by the <paramref name="definition"/>, 
        /// and adds any discovered issues to <paramref name="issues"/>
        /// </summary>
        /// <param name="definition">The definition containing properties</param>
        /// <param name="syntax">The syntax structure to check</param>
        /// <param name="issues">The issues discovered</param>
        private static void PropertyCheck(SemanticModel model, UMLSpecification.IPropertyHost definition, BaseTypeDeclarationSyntax syntax, List<string> issues)
        {
            // Get the symbol corresponding to the syntax object
            var symbol = model.GetDeclaredSymbol(syntax);

            // Grab current symbol's properties' symbols
            var propertySymbols = symbol.GetMembers().OfType<IPropertySymbol>().ToList();

            // Add those of the base classes (if any)
            var baseClassSymbol = symbol.BaseType;
            while (baseClassSymbol != null)
            {
                propertySymbols.AddRange(baseClassSymbol.GetMembers().OfType<IPropertySymbol>());
                baseClassSymbol = baseClassSymbol.BaseType;
            }

            // Check all properties
            foreach (var property in definition.Properties)
            {
                var propertySymbol = propertySymbols
                    .Where(p => p.Name == property.Name)
                    .FirstOrDefault();

                if (propertySymbol is null)
                {
                    issues.Add($"Expected {definition.Kind} {definition.Name} to have a property named {property.Name}.");
                }
                else
                {                    
                    // check visibility
                    if (!(syntax is InterfaceDeclarationSyntax) && !VisibilityCheck(property.Visibility, propertySymbol))
                    {
                        issues.Add($"Expected {definition.Kind} {definition.Name} property {property.Name} to be declared {property.Visibility}");
                    }

                    // Check abstract status 
                    if (property.Abstract && !propertySymbol.IsAbstract)
                    {
                        issues.Add($"Expected {definition.Kind} {definition.Name} property {property.Name} to be declared abstract");
                    }

                    // check type 
                    if (property.Type != propertySymbol.Type.ToMinimalDisplayString(model, syntax.SpanStart))
                    {
                        issues.Add($"Expected {definition.Kind} {definition.Name} property {property.Name} to have Type {property.Type}");
                    }

                    // check getter 
                    if (property.Get && propertySymbol.GetMethod == null)
                    {
                        issues.Add($"Expected {definition.Kind} {definition.Name} property {property.Name} to have a get method.");
                    }

                    // check setter
                    if (property.Set && propertySymbol.SetMethod == null)
                    {
                        issues.Add($"Expected {definition.Kind} {definition.Name} property {property.Name} to have a set method.");
                    }
                }
            }
        }

        private static void MethodCheck(UMLSpecification.IMethodHost definition, BaseTypeDeclarationSyntax syntax, List<string> issues)
        {
            // Check all methods
            foreach (var method in definition.Methods)
            {
                var methodSyntax = syntax.DescendantNodes()
                        .OfType<MethodDeclarationSyntax>()
                        .Where(pds =>
                        {
                            // Because we can have multiple method overloads, we need to use the 
                            // full signature for identifying the method
                            if (!pds.Identifier.ValueText.Equals(method.Name)) return false;
                            if (!pds.ReturnType.ToString().Equals(method.ReturnType)) return false;
                            if (pds.ParameterList.Parameters.Count != method.Parameters.Count) return false;
                            for (int i = 0; i < method.Parameters.Count; i++)
                            {
                                var parameterDef = method.Parameters[i];
                                var parameterSyntax = pds.ParameterList.Parameters[i];
                                if (parameterDef.Name != parameterSyntax.Identifier.ValueText) return false;
                                if (parameterDef.Type != parameterSyntax.Type.ToString()) return false;
                            }
                            return true;
                        })
                        .FirstOrDefault();
                if (methodSyntax == null)
                {
                    var paramStr = String.Join(',', method.Parameters.Select(param => $"{param.Name}:{param.Type}").ToArray());
                    issues.Add($"Expected {definition.Kind} {definition.Name} to have a method named {method.Name} with parameters ({paramStr}) and return type {method.ReturnType}.");
                }
                else
                {
                    // check visibility
                    if (!VisibilityCheck(method.Visibility, methodSyntax))
                    {
                        issues.Add($"Expected {definition.Kind} {definition.Name} method to be declared {method.Visibility}");
                    }
                }
            }
        }

        /// <summary>
        /// Helper method for checking the visibility of a declared type
        /// </summary>
        /// <param name="visibility">The expected visibility declared in the UML</param>
        /// <param name="declarationSyntax">The declared type to check</param>
        /// <returns></returns>
        private static bool VisibilityCheck(UMLSpecification.Visibility visibility, MemberDeclarationSyntax declarationSyntax)
        {
            if (visibility == UMLSpecification.Visibility.Public && !declarationSyntax.Modifiers.Any(token => token.Text == "public"))
            {
                return false;
            }
            if (visibility == UMLSpecification.Visibility.Protected && !declarationSyntax.Modifiers.Any(token => token.Text == "protected"))
            {
                return false;
            }
            if (visibility == UMLSpecification.Visibility.Private && !declarationSyntax.Modifiers.Any(token => token.Text == "private"))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Helper method for checking the visibility of a declared symbol
        /// </summary>
        /// <param name="visibility">The expected visibility declared in the UML</param>
        /// <param name="declarationSyntax">The declared type to check</param>
        /// <returns></returns>
        private static bool VisibilityCheck(UMLSpecification.Visibility visibility, ISymbol symbol)
        {
            if (visibility == UMLSpecification.Visibility.Public && !(symbol.DeclaredAccessibility == Accessibility.Public))
            {
                return false;
            }
            if (visibility == UMLSpecification.Visibility.Protected && !(symbol.DeclaredAccessibility == Accessibility.Protected))
            {
                return false;
            }
            if (visibility == UMLSpecification.Visibility.Private && !(symbol.DeclaredAccessibility == Accessibility.Private))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if the syntax element is declared abstract
        /// </summary>
        /// <param name="declarationSyntax">The declaration syntax to check</param>
        /// <returns>True if it is abstract, false if not</returns>
        private static bool AbstractCheck(MemberDeclarationSyntax declarationSyntax)
        {
            return declarationSyntax.Modifiers.Any(m => m.IsKind(SyntaxKind.AbstractKeyword));
        }

        private static bool CompareAliasedTypes(string aliasType, string actualType)
        {
            return aliasType switch
            {
                "IEnumerable<string>" => actualType == "IEnumerable<String>",
                "bool" => actualType == "Boolean",
                "string" => actualType == "String",
                "sbyte" => actualType == "SByte",
                "byte" => actualType == "Byte",
                "short" => actualType == "Int16",
                "ushort" => actualType == "UInt16",
                "int" => actualType == "Int32",
                "uint" => actualType == "UInt32",
                "long" => actualType == "Int64",
                "ulong" => actualType == "UInt64",
                "float" => actualType == "Single",
                "double" => actualType == "Double",
                "decimal" => actualType == "Decimal",
                _ => actualType == aliasType
            };
        } 

    }
}
