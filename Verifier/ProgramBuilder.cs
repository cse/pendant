﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSU.CS.ProgramVerifier
{
    public static class ProgramBuilder
    {
        public static async Task Build(Solution solution)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = "cmd.exe";
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;

            using (var process = Process.Start(psi))
            {
                using (StreamWriter writer = process.StandardInput)
                {
                    if (writer.BaseStream.CanWrite)
                    {
                        writer.WriteLine($"cd ${solution.FilePath}");
                        writer.WriteLine($"dotnet build");
                    }
                }
                await process.WaitForExitAsync();
            }
        }
    }
}
