﻿using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.MSBuild;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSU.CS.ProgramVerifier
{
    public static class Verifier
    {
        /// <summary>
        /// Registers default values for MSBuildLocator before the verifier uses 
        /// any MS Build processes
        /// </summary>
        static Verifier()
        {
            MSBuildLocator.RegisterDefaults();
        }

        public static async Task<AnalysisResult> Check(string studentPath, string specificationPath)
        {
            var specification = await Specification.LoadAsync(specificationPath);

            var result = new AnalysisResult();

            // Compile the student solution using MSBuild
            using (var workspace = MSBuildWorkspace.Create())
            {
                workspace.LoadMetadataForReferencedProjects = true;

                var solution = await workspace.OpenSolutionAsync(Path.Combine(studentPath, $"{specification.Structure.Name}.sln"));

                // Restore any NuGet packages
                //await NugetManager.Restore(solution);

                // Run the diagonostic analyzers on the student project
                // including custom ones that detect design and style issues
                await DiagnosticRunner.Run(solution, result);

                // Iterate over each project in the specification and verify independently
                foreach (var spec in specification.Structure.Projects)
                {
                    var project = solution.Projects.Where(project => project.Name.Equals(spec.Name)).FirstOrDefault();

                    if (project == null)
                    {
                        result.StructuralIssues.Add($"Expected solution to contain a project named {spec.Name}");
                    }
                    else
                    {
                        // Verify the student project meets the specified structure 
                        // This can provide helpful structural information even when the project cannot
                        // be completely compilied
                        await StructureVerifier.CheckProjectStructure(spec, project, result);
                    }
                }

                // Run any student tests
                //await TestRunner.Run(solution, result);

                result.FunctionalIssues.Add("Pendant is unable to check for functional issues at this time.");
                await System.IO.File.AppendAllTextAsync(@"c:\\workDirectory\log.txt", $"Finished validating ${studentPath}");

                /*
                try
                {
                    await System.IO.File.AppendAllTextAsync(@"c:\\workDirectory\log.txt", $"Getting ready to run tests on ${studentPath}; {result.StructuralIssues.Count} structural issues found; Compilation was {result.DoesCompile}");
                    // Run any specification tests
                    if (result.StructuralIssues.Count == 0) await SpecificationTestRunner.Run(specification, workspace, result);
                    else result.FunctionalIssues.Add("Functional issues were not checked for as structural issues must be fixed first");
                }
                catch(Exception e)
                {
                    await System.IO.File.AppendAllTextAsync(@"c:\\workDirectory\log.txt", $"Encountered Error: ${e.Message} ${e.StackTrace}");
                }
                */
            }

            return result;
        }
    }
}
