﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using CodeAnalyzers.Test;

/*
namespace KSU.CS.Pendant.CodeAnalyzers.Test
{
    [TestClass]
    public class CodeAnalyzersUnitTest
    {
        //No diagnostics expected to show up
        [TestMethod]
        public async Task TestMethod1()
        {
            var test = @"";

            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public async Task TestMethod2()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        class {|#0:TypeName|}
        {   
        }
    }";

            var expected = CSharpAnalyzerVerifier<CommentsAnalyzer>.Diagnostic("CodeAnalyzers").WithLocation(0).WithArguments("TypeName");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }
    }
}
*/
