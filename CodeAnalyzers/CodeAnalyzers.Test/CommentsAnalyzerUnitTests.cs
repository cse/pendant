﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using KSU.CS.Pendant.CodeAnalysis.StyleAnalyzers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Testing;
using CodeAnalyzers.Test;
using VerifyCS = CodeAnalyzers.Test.CSharpAnalyzerVerifier<
   KSU.CS.Pendant.CodeAnalysis.StyleAnalyzers.CommentsAnalyzer>;

namespace KSU.CS.Pendant.CodeAnalysis.Test
{
    [TestClass]
    public class CommentsAnalyzerTests
    {
        #region Class Commenting Tests

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedClass()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {             
        }
    }";      
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestClassMissingComments()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class {|#0:Foo|}
        {             
        }
    }";

            var expected = VerifyCS.Diagnostic("XMLC0001").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestClassWithNoSummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <remarks>
        /// XML, but not a summary
        /// </remarks>
        class {|#0:Foo|}
        {             
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0001").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestClassWithEmptySummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// </summary>
        class {|#0:Foo|}
        {             
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0001").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        #endregion

        #region Struct Commenting Tests 

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedStruct()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        struct Foo
        {             
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestStructMissingComments()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        struct {|#0:Foo|}
        {             
        }
    }";

            var expected = VerifyCS.Diagnostic("XMLC0002").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestStructWithNoSummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <remarks>
        /// XML, but not a summary
        /// </remarks>
        struct {|#0:Foo|}
        {             
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0002").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestStructWithEmptySummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// </summary>
        struct {|#0:Foo|}
        {             
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0002").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        #endregion

        #region Interface Commenting Tests 

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedInterface()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        interface Foo
        {             
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestInterfaceMissingComments()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        interface {|#0:Foo|}
        {             
        }
    }";

            var expected = VerifyCS.Diagnostic("XMLC0003").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestInterfaceWithNoSummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <remarks>
        /// XML, but not a summary
        /// </remarks>
        interface {|#0:Foo|}
        {             
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0003").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestInterfaceWithEmptySummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// </summary>
        interface {|#0:Foo|}
        {             
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0003").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        #endregion

        #region Enum Commenting Tests 

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedEnum()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        enum Foo
        {
            A, B, C   
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestEnumMissingComments()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        enum {|#0:Foo|}
        {
            A, B, C   
        }
    }";

            var expected = VerifyCS.Diagnostic("XMLC0004").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestEnumWithNoSummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <remarks>
        /// XML, but not a summary
        /// </remarks>
        enum {|#0:Foo|}
        {
            A, B, C   
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0004").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestEnumWithEmptySummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// </summary>
        enum {|#0:Foo|}
        {
            A, B, C
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0004").WithLocation(0).WithArguments("Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        #endregion

        #region Field Commenting Tests 

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedField()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// Sample int 
            /// </summary>
            public int bar = 3;
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestFieldMissingComments()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            {|#0:public int bar = 3;|}
        }
    }";

            var expected = VerifyCS.Diagnostic("XMLC0005").WithLocation(0).WithArguments("bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestFieldWithNoSummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <remarks>
            /// Remarks, but no summary
            /// </remarks>
            {|#0:public int bar = 3;|}
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0005").WithLocation(0).WithArguments("bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestFieldWithEmptySummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            ///
            /// </summary>
            {|#0:public int bar = 3;|}
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0005").WithLocation(0).WithArguments("bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        #endregion


        #region Property Commenting Tests 

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedProperty()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// The bar is...
            /// </summary>
            int Bar { get; set; }
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestPropertyMissingComments()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            int {|#0:Bar|} { get; set; }
        }
    }";

            var expected = VerifyCS.Diagnostic("XMLC0006").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestPropertyWithNoSummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <remark>
            /// The bar is...
            /// </remark>
            int {|#0:Bar|} { get; set; }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0006").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestPropertyWithEmptySummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// </summary>
            int {|#0:Bar|} { get; set; }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0006").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        #endregion

        #region Method Commenting Tests 

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedMethodInClass()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// The bar is...
            /// </summary>
            /// <param name=""a"">A number</param>
            /// <param name=""b"">A boolean</param>
            /// <returns>another number</returns>
            int Bar(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedMethodInInterface()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        interface Foo
        {
            /// <summary>
            /// The bar is...
            /// </summary>
            /// <param name=""a"">A number</param>
            /// <param name=""b"">A boolean</param>
            /// <returns>another number</returns>
            int Bar(int a, bool b);
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestProperlyCommentedMethodInStruct()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        struct Foo
        {
            /// <summary>
            /// The bar is...
            /// </summary>
            /// <param name=""a"">A number</param>
            /// <param name=""b"">A boolean</param>
            /// <returns>another number</returns>
            int Bar(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestMethodMissingCommentsInClass()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";

            var expected1 = VerifyCS.Diagnostic("XMLC0007").WithLocation(0).WithArguments("Bar");
            var expected2 = VerifyCS.Diagnostic("XMLC0008").WithLocation(0).WithArguments("Bar", "a");
            var expected3 = VerifyCS.Diagnostic("XMLC0008").WithLocation(0).WithArguments("Bar", "b");
            var expected4 = VerifyCS.Diagnostic("XMLC0009").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected1, expected2, expected3, expected4);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestMethodMissingCommentsInStruct()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        struct Foo
        {
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";

            var expected1 = VerifyCS.Diagnostic("XMLC0007").WithLocation(0).WithArguments("Bar");
            var expected2 = VerifyCS.Diagnostic("XMLC0008").WithLocation(0).WithArguments("Bar", "a");
            var expected3 = VerifyCS.Diagnostic("XMLC0008").WithLocation(0).WithArguments("Bar", "b");
            var expected4 = VerifyCS.Diagnostic("XMLC0009").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected1, expected2, expected3, expected4);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestMethodWithNoSummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <param name=""a"">A number</param>
            /// <param name=""b"">A boolean</param>
            /// <returns>another number</returns>
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0007").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestMethodWithEmptySummary()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name=""a"">A number</param>
            /// <param name=""b"">A boolean</param>
            /// <returns>another number</returns>
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0007").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestMethodWithMissingParam()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// A summary
            /// </summary>            
            /// <param name=""b"">A boolean</param>
            /// <returns>another number</returns>
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0008").WithLocation(0).WithArguments("Bar", "a");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestMethodWithEmptyParam()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// A summary
            /// </summary>            
            /// <param name=""a"">An int</param>
            /// <param name=""b""></param>
            /// <returns>another number</returns>
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0008").WithLocation(0).WithArguments("Bar", "b");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public async Task TestMethodWithMissingReturns()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// A summary
            /// </summary>            
            /// <param name=""a"">An int</param>
            /// <param name=""b"">A bool</param>
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0009").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMethodWithEmptyReturns()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// A summary
            /// </summary>            
            /// <param name=""a"">An int</param>
            /// <param name=""b"">A bool</param>
            /// <returns></returns>
            int {|#0:Bar|}(int a, bool b) 
            {
                if(b) return a;
                else return a + 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0009").WithLocation(0).WithArguments("Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMethodWithVoidReturns()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// A summary
            /// </summary>            
            /// <param name=""a"">An int</param>
            /// <param name=""b"">A bool</param>
            void Bar(int a, bool b) 
            {
            }
        }
    }";
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test);
        }


        #endregion

        [TestMethod]
        public async Task TestMalformedClassXmlComment()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary
        /// Sample Summary
        /// </summary>
        class {|#0:Foo|}
        {
            /// <summary>
            /// A summary
            /// </summary>            
            /// <param name=""a"">An int</param>
            /// <param name=""b"">A bool</param>
            /// <returns>the number 5</returns>
            int Bar(int a, bool b) 
            {
                return 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0010").WithLocation(0).WithArguments("class", "Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMalformedMethodXmlComment()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summary>
            /// A summary
            /// </summary>            
            /// <param name=""a"">An int</param>
            /// <param name=""b"">A bool</param>
            /// <returns>the number 5</return>
            int {|#0:Bar|}(int a, bool b) 
            {
                return 5;
            }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0010").WithLocation(0).WithArguments("method", "Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMalformedPropertyXmlComment()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summmary>
            /// A summary
            /// </summary>
            int {|#0:Bar|} { get; set; }
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0010").WithLocation(0).WithArguments("property", "Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMalformedFieldXmlComment()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class Foo
        {
            /// <summmary>
            /// A summary
            /// </summary>
            {|#0:public int Bar = 5;|}
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0010").WithLocation(0).WithArguments("field", "Bar");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMalformedEnumXmlComment()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// </summary>
        /// Sample Summary
        /// </summary>
        enum {|#0:Foo|}
        {
            A, B, C
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0010").WithLocation(0).WithArguments("enum", "Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMalformedInterfaceXmlComment()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// </summary>
        /// Sample Summary
        /// </summary>
        interface {|#0:Foo|}
        {
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0010").WithLocation(0).WithArguments("interface", "Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task TestMalformedStructXmlComment()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        /// </summary>
        /// Sample Summary
        /// </summary>
        struct {|#0:Foo|}
        {
        }
    }";
            var expected = VerifyCS.Diagnostic("XMLC0010").WithLocation(0).WithArguments("struct", "Foo");
            await CSharpAnalyzerVerifier<CommentsAnalyzer>.VerifyAnalyzerAsync(test, expected);
        }


        /*
        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on fields
        /// </summary>
        [TestMethod]
        public void TestForProperlyCommentedFields()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            public int test;
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Fields must have an xml summary comment."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 16, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on interfaces
        /// </summary>
        [TestMethod]
        public void TestForProperlyCommentedInterfaces()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            public interface Itest {}
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Interfaces must have an xml summary comment."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 16, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on structs
        /// </summary>
        [TestMethod]
        public void TestForProperlyCommentedStructs()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            struct Test 
            {

            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Structs must have an xml summary comment."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 16, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on Enums
        /// </summary>
        [TestMethod]
        public void TestForProperlyCommentedEnums()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            enum Test 
            {
                Low,
                Medium,
                High
            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Enums must have an xml summary comment."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 16, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on classes
        /// </summary>
        [TestMethod]
        public void TestForProperlyCommentedClasses()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        class TypeName
        {   

        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Classes must have an xml summary comment."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 11, 9)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on methods
        /// </summary>
        [TestMethod]
        public void TestForProperlyCommentedMethods()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            public void Test()
            {
                break;
            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Methods must have an xml summary comment."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 16, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on properties
        /// </summary>
        [TestMethod]
        public void TestForCommentsInProperties()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            /// <summary>
            /// 
            /// </summary>
            public int Agedfvcvxv { get { return Age; } set { value = Age; } }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Must include a summary in xml summary comment with no extra new lines."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 19, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on fields
        /// </summary>
        [TestMethod]
        public void TestForCommentsInFields()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            /// <summary>
            /// 
            /// </summary>
            public int test;
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Must include a summary in xml summary comment with no extra new lines."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 19, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on interfaces
        /// </summary>
        [TestMethod]
        public void TestForCommentsInInterfaces()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            /// <summary>
            /// 
            /// </summary>
            public interface Itest {}
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Must include a summary in xml summary comment with no extra new lines."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 19, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on structs
        /// </summary>
        [TestMethod]
        public void TestForCommentsInStructs()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            /// <summary>
            /// 
            /// </summary>
            struct Test 
            {

            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Must include a summary in xml summary comment with no extra new lines."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 19, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on Enums
        /// </summary>
        [TestMethod]
        public void TestForCommentsInEnums()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            /// <summary>
            /// 
            /// </summary>
            enum Test 
            {
                Low,
                Medium,
                High
            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Must include a summary in xml summary comment with no extra new lines."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 19, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on classes
        /// </summary>
        [TestMethod]
        public void TestForCommentsInClasses()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// 
        /// </summary>
        class TypeName
        {   

        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Must include a summary in xml summary comment with no extra new lines."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 14, 9)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on methods
        /// </summary>
        [TestMethod]
        public void TestForCommentsInMethods()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            /// <summary>
            /// 
            /// </summary>
            public void Test()
            {
                break;
            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Must include a summary in xml summary comment with no extra new lines."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 19, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        /// <summary>
        /// Verifies whether the Comments Analyzer can detect proper XML commenting conventions on methods
        /// </summary>
        [TestMethod]
        public void TestForCommentsInMethodsForParameters()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        /// <summary>
        /// Sample Summary
        /// </summary>
        class TypeName
        {   
            /// <summary>
            /// fdsf
            /// </summary>
            /// <param name=""test""></param>
            public void Test(int test)
            {
                break;
            }
        }
    }";
            var expected = new DiagnosticResult
            {
                Id = "Comments",
                Message = String.Format("Violation: Parameters must have a definition in an xml summary comment."),
                Severity = DiagnosticSeverity.Warning,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 20, 13)
                    }
            };

            VerifyCSharpDiagnostic(test, expected);
        }
        */
    }
}