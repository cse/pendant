﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using VerifyCS = CodeAnalyzers.Test.CSharpAnalyzerVerifier<
   KSU.CS.Pendant.CodeAnalysis.DesignAnalyzers.PropertySelfReferenceAnalyzer>;

namespace KSU.CS.Pendant.CodeAnalysis.Test
{
    [TestClass]
    public class PropertySelfReferenceAnalyzerUnitTest
    {
        //No diagnostics expected to show up
        [TestMethod]
        public async Task NotFoundInAutoProperty()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class TestClass
        {   
            public int Number { get; set; } = 0;
        }
    }
";

            await VerifyCS.VerifyAnalyzerAsync(test);
        }


        //No diagnostics expected to show up
        [TestMethod]
        public async Task NotFoundInProperPrivateBackingVariableProperty()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class TestClass
        {   
            private int _number = 0;
            public int Number 
            { 
                get { return _number; }
                set { _number = value; }
            }
        }
    }
";
            await VerifyCS.VerifyAnalyzerAsync(test);
        }

        //No diagnostics expected to show up
        [TestMethod]
        public async Task NotFoundWithSharedTypeIdentifier()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        enum Stuff { One, Two, Three }

        class TestClass
        {   
            public Stuff Stuff 
            { 
                get 
                { 
                    Stuff stuff = Stuff.One;
                    return stuff;
                }                
            }
        }
    }
";
            await VerifyCS.VerifyAnalyzerAsync(test);
        }

        //Diagnostic triggered in getter
        [TestMethod]
        public async Task SelfReferenceInGetter()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        class TestClass 
        {   
            public int {|#0:Number|} 
            {
                get { return {|#1:Number|}; }
            }
        }
    }";

            var expected = VerifyCS.Diagnostic("FAL0001").WithLocation(0).WithLocation(1).WithArguments("Number", "get");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        //Diagnostic triggered in setter
        [TestMethod]
        public async Task SelfReferenceInSetter()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        class TestClass 
        {   
            public int {|#0:Number|} 
            {
                set { {|#1:Number|} = value; }
            }
        }
    }";

            var expected = VerifyCS.Diagnostic("FAL0001").WithLocation(0).WithLocation(1).WithArguments("Number", "set");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        // Diagnostic triggered with lambda get syntax
        [TestMethod]
        public async Task SelfReferenceInLambdaProperty()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        class TestClass 
        {   
            public int {|#0:Number|} 
            {
                get => {|#1:Number|}; 
            }
        }
    }";

            var expected = VerifyCS.Diagnostic("FAL0001").WithLocation(0).WithLocation(1).WithArguments("Number", "get");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        // Diagnostic triggered with lambda property syntax
        [TestMethod]
        public async Task SelfReferenceInLambdaGetter()
        {
            var test = @"
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;

    namespace ConsoleApplication1
    {
        class TestClass 
        {   
            public int {|#0:Number|} => {|#1:Number|}; 
        }
    }";

            var expected = VerifyCS.Diagnostic("FAL0001").WithLocation(0).WithLocation(1).WithArguments("Number", "get");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }
    }
}
