﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using VerifyCS = CodeAnalyzers.Test.CSharpAnalyzerVerifier<
   KSU.CS.Pendant.CodeAnalysis.StyleAnalyzers.NamingConventionsAnalyzer>;

namespace KSU.CS.Pendant.CodeAnalysis.Test
{
    [TestClass]
    public class NamingConventionsAnalyzerUnitTests
    {
        [TestMethod]
        public async Task ValidNamespaceNamesPass()
        {
            var test = @"
    namespace Valid.Example.OfANamespace
    {
        class TestClass
        {               
        }
    }
";
            await VerifyCS.VerifyAnalyzerAsync(test);
        }


        [TestMethod]
        public async Task NamespaceFirstSegmentMustBePascalCase()
        {
            var test = @"
    namespace {|#0:invalid.Example|}
    {
        class TestClass
        {               
            int Foo() 
            {
                var f = 5;
                return f;
            }
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0001").WithLocation(0).WithArguments("invalid.Example", "invalid");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task LastNamespaceSegmentsMustBePascalCase()
        {
            var test = @"
    namespace {|#0:Invalid.example|}
    {
        class TestClass
        {               
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0001").WithLocation(0).WithArguments("Invalid.example", "example");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task ClassNamesMustBePascalCase()
        {
            var test = @"
    namespace Example
    {
        public class {|#0:testClass|}
        {               
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0002").WithLocation(0).WithArguments("testClass");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task ClassesShouldNotBeNamedLikeInterfaces()
        {
            var test = @"
    namespace Example
    {
        public class {|#0:ITestClass|}
        {               
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0003").WithLocation(0).WithArguments("ITestClass", 'T');
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task StructNamesMustBePascalCase()
        {
            var test = @"
    namespace Example
    {
        public struct {|#0:testStruct|}
        {               
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0004").WithLocation(0).WithArguments("testStruct");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task StructsShouldNotBeNamedLikeInterfaces()
        {
            var test = @"
    namespace Example
    {
        public struct {|#0:ITestStruct|}
        {               
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0005").WithLocation(0).WithArguments("ITestStruct", 'T');
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task EnumNamesMustBePascalCase()
        {
            var test = @"
    namespace Example
    {
        public enum {|#0:testEnum|}
        {               
            One, Two, Three
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0006").WithLocation(0).WithArguments("testEnum");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task EnumsShouldNotBeNamedLikeInterfaces()
        {
            var test = @"
    namespace Example
    {
        public enum {|#0:ITestEnum|}
        {               
            One, Two, Three
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0007").WithLocation(0).WithArguments("ITestEnum", 'T');
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task InterfacesShouldStartWithI()
        {
            var test = @"
    namespace Example
    {
        public interface {|#0:TestInterface|}
        {               
            
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0008").WithLocation(0).WithArguments("TestInterface");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task InterfacesShouldUsePascalCase()
        {
            var test = @"
    namespace Example
    {
        public interface {|#0:ItestInterface|}
        {               
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0009").WithLocation(0).WithArguments("ItestInterface", 'I');
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }


        [TestMethod]
        public async Task MethodsShouldUsePascalCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            public void {|#0:doSomething|}() {}
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0010").WithLocation(0).WithArguments("doSomething");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task PropertiesShouldUsePascalCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            public string {|#0:info|} {get; set;}
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0011").WithLocation(0).WithArguments("info");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task ParametersShouldUseCamelCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            public int TestMethod(int {|#0:Num|})
            {
                return Num + 5;
            }
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0012").WithLocation(0).WithArguments("Num");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task PublicConstFieldsShouldUsePascalCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            public const double {|#0:pi|} = 3.14;
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0013").WithLocation(0).WithArguments("pi");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task PrivateFieldsShouldBePrefixedWithUnderscore()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            private double {|#0:pi|} = 3.14;
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0014").WithLocation(0).WithArguments("pi");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task ProtectedFieldsShouldBePrefixedWithUnderscore()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            protected double {|#0:pi|} = 3.14;
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0014").WithLocation(0).WithArguments("pi");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task PrivateFieldsShouldUseCamelCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            private double {|#0:_Pi|} = 3.14;
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0015").WithLocation(0).WithArguments("_Pi");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task ProtectedFieldsShouldUseCamelCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            private double {|#0:_Pi|} = 3.14;
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0015").WithLocation(0).WithArguments("_Pi");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task LocalVariablesShouldUseCamelCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            private void DoSomething() 
            {
                int {|#0:Local|} = 5;
            }
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0016").WithLocation(0).WithArguments("Local");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task MultipleLocalVariablesShouldUseCamelCase()
        {
            var test = @"
    namespace Example
    {
        public class TestClass
        {               
            private void DoSomething() 
            {
                int num1 = 0, num2 = 4, {|#0:Num3|} = 5;
            }
        }
    }
";
            var expected = VerifyCS.Diagnostic("NAM0016").WithLocation(0).WithArguments("Num3");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

    }
}
