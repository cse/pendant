﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using VerifyCS = CodeAnalyzers.Test.CSharpAnalyzerVerifier<
   KSU.CS.Pendant.CodeAnalysis.DesignAnalyzers.NewInMemberAnalyzer>;

namespace KSU.CS.Pendant.CodeAnalysis.Test
{
    [TestClass]
    public class NewInMemberAnalyzerUnitTest
    {


        [TestMethod]
        public async Task NewNotFoundInMethod()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class BaseClass 
        {
            public virtual void Foo() { }
        }
        class DerivedClass : BaseClass
        {   
            public override void Foo() { }
        }
    }
";

            await VerifyCS.VerifyAnalyzerAsync(test);
        }

        [TestMethod]
        public async Task NewFoundInMethod()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class BaseClass 
        {
            public void Foo() { }
        }
        class DerivedClass : BaseClass
        {   
            {|#0:public new void Foo() { }|}
        }
    }
";
            var expected = VerifyCS.Diagnostic("FAL0002").WithLocation(0).WithArguments("Foo");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }

        [TestMethod]
        public async Task NewNotFoundInProperty()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class BaseClass 
        {
            public virtual int Foo { get { return 4; } }
        }
        class DerivedClass : BaseClass
        {   
            public override int Foo { get { return 5; } }
        }
    }
";

            await VerifyCS.VerifyAnalyzerAsync(test);
        }

        [TestMethod]
        public async Task NewFoundInProperty()
        {
            var test = @"
    namespace ConsoleApplication1
    {
        class BaseClass 
        {
            public int Foo { get { return 4; } }
        }
        class DerivedClass : BaseClass
        {   
            {|#0:public new int Foo { get { return 5; } }|}
        }
    }
";
            var expected = VerifyCS.Diagnostic("FAL0003").WithLocation(0).WithArguments("Foo");
            await VerifyCS.VerifyAnalyzerAsync(test, expected);
        }
    }
}
