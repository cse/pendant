﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System.Collections.Immutable;
using System.Linq;
using CodeAnalyzers;

namespace KSU.CS.Pendant.CodeAnalysis.DesignAnalyzers
{
    /// <summary>
    /// A Code analyzer that checks for properties that reference themselves, i.e.:
    /// <code>
    /// public string Foo {
    ///   get { return Foo; }
    ///   set { Foo = value; }
    /// }
    /// </code>
    /// This kind of self-reference creates and infinite recursion when the set or get method is invoked at runtime, 
    /// causing a StackOverflow
    /// </summary>
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class PropertySelfReferenceAnalyzer : DiagnosticAnalyzer
    {
        #region Metadata 

        // Property Self-Reference metadata
        public const string PSRDiagnosticId = "FAL0001";
        private static readonly LocalizableString PSRTitle = new LocalizableResourceString(nameof(Resources.PropertySelfReferenceAnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString PSRMessageFormat = new LocalizableResourceString(nameof(Resources.PropertySelfReferenceAnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString PSRDescription = new LocalizableResourceString(nameof(Resources.PropertySelfReferenceAnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private const string PSRCategory = "Design";
        private static readonly DiagnosticDescriptor PropertySelfReferenceRule = new DiagnosticDescriptor(PSRDiagnosticId, PSRTitle, PSRMessageFormat, PSRCategory, DiagnosticSeverity.Error, isEnabledByDefault: true, description: PSRDescription, helpLinkUri: $"https://pendant.cs.ksu.edu/diagnostic/{PSRDiagnosticId}");

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(PropertySelfReferenceRule); } }

        #endregion

        /// <summary>
        /// Initializes the PropertySelfReferenceAnalyzer
        /// </summary>
        /// <param name="context">The analysis context</param>
        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
        }

        /// <summary>
        /// Analyzes a PropertyDeclarationSyntax Node for self-reference errors
        /// </summary>
        /// <param name="context">The context of the PropertyDeclarationSyntax Node</param>
        private static void AnalyzeNode(SyntaxNodeAnalysisContext context)
        {
            // The context Node should be a PropertyDeclarationSyntax
            var propertyDeclaration = (PropertyDeclarationSyntax)context.Node;

            // The symbol corresponding to the property
            var propertySymbol = context.SemanticModel.GetDeclaredSymbol(propertyDeclaration);

            // Find all identifiers that exist within the property declaration
            var identifiers = propertyDeclaration.DescendantNodes()
                .OfType<IdentifierNameSyntax>();
            foreach (var identifier in identifiers)
            {
                // Get the symbol that corresponds to the identifier
                var symbolInfo = context.SemanticModel.GetSymbolInfo(identifier);
                // Check that the identifier symbol is not the property symbol
                if (SymbolEqualityComparer.Default.Equals(propertySymbol, symbolInfo.Symbol))
                {
                    // Determine if the self-reference occured in a getter or setter
                    AccessorDeclarationSyntax accessor = identifier.Ancestors()
                        .OfType<AccessorDeclarationSyntax>()
                        .FirstOrDefault();
                    if (accessor != null && accessor.IsKind(SyntaxKind.SetAccessorDeclaration))
                    {
                        // Create the diagnostic
                        var primaryLocation = propertyDeclaration.Identifier.GetLocation();
                        var additionalLocations = new Location[] { identifier.GetLocation() };
                        var diagnostic = Diagnostic.Create(
                            PropertySelfReferenceRule,
                            primaryLocation,
                            additionalLocations,
                            new string[] { propertyDeclaration.Identifier.ValueText, "set" }
                            );
                        context.ReportDiagnostic(diagnostic);
                    }
                    else
                    {
                        // Create the diagnostic
                        var primaryLocation = propertyDeclaration.Identifier.GetLocation();
                        var additionalLocations = new Location[] { identifier.GetLocation() };
                        var diagnostic = Diagnostic.Create(
                            PropertySelfReferenceRule,
                            primaryLocation,
                            additionalLocations,
                            new string[] { propertyDeclaration.Identifier.ValueText, "get" }
                            );
                        context.ReportDiagnostic(diagnostic);
                    }
                }
            }
        }
    }
}