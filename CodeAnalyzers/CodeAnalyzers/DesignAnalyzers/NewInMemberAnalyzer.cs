﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using CodeAnalyzers;

namespace KSU.CS.Pendant.CodeAnalysis.DesignAnalyzers
{
    /// <summary>
    /// A Code analyzer that checks for methods and properties declared with the 'new' keyword, i.e.:
    /// 
    /// <code>
    /// public class BaseClass 
    /// {
    ///   public int Foo => 3;
    /// }
    /// public class DerivedClass 
    /// {
    ///    public new int Foo => 5;
    /// }
    /// </code>
    /// 
    /// This should almost always be replaced with virtual/override keywords in 
    /// the base and derived classes
    /// </summary>
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class NewInMemberAnalyzer : DiagnosticAnalyzer
    {
        #region Metadata 

        // Use of new keyword in method metadata
        public const string NewInMethodDiagnosticId = "FAL0002";
        private static readonly LocalizableString NewInMethodTitle = new LocalizableResourceString(nameof(Resources.NewInMethodAnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString NewInMethodMessageFormat = new LocalizableResourceString(nameof(Resources.NewInMethodAnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString NewInMethodDescription = new LocalizableResourceString(nameof(Resources.NewInMethodAnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private const string NewInMethodCategory = "Design";
        private static readonly DiagnosticDescriptor NewInMethodRule = new DiagnosticDescriptor(NewInMethodDiagnosticId, NewInMethodTitle, NewInMethodMessageFormat, NewInMethodCategory, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: NewInMethodDescription, helpLinkUri: $"https://pendant.cs.ksu.edu/diagnostic/{NewInMethodDiagnosticId}");

        // Use of new keyword in property metadata
        public const string NewInPropertyDiagnosticId = "FAL0003";
        private static readonly LocalizableString NewInPropertyTitle = new LocalizableResourceString(nameof(Resources.NewInPropertyAnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString NewInPropertyMessageFormat = new LocalizableResourceString(nameof(Resources.NewInPropertyAnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString NewInPropertyDescription = new LocalizableResourceString(nameof(Resources.NewInPropertyAnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private const string NewInPropertyCategory = "Design";
        private static readonly DiagnosticDescriptor NewInPropertyRule = new DiagnosticDescriptor(NewInPropertyDiagnosticId, NewInPropertyTitle, NewInPropertyMessageFormat, NewInPropertyCategory, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: NewInPropertyDescription, helpLinkUri: $"https://pendant.cs.ksu.edu/diagnostic/{NewInPropertyDiagnosticId}");

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(NewInMethodRule, NewInPropertyRule); } }

        #endregion

        /// <summary>
        /// Initializes the NewInMethodAnalyzer
        /// </summary>
        /// <param name="context">The analysis context</param>
        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(AnalyzePropertyNode, SyntaxKind.PropertyDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeMethodNode, SyntaxKind.MethodDeclaration);
        }

        /// <summary>
        /// Analyzes a MethodDeclarationSyntax Node for use of the 'new' keyword
        /// </summary>
        /// <param name="context">The context of the MethodDeclarationSyntax Node</param>
        private static void AnalyzeMethodNode(SyntaxNodeAnalysisContext context)
        {
            var declaration = context.Node as MethodDeclarationSyntax;
            var modifiers = declaration.Modifiers;
            if (modifiers.Any(m => m.IsKind(SyntaxKind.NewKeyword)))
            {
                // Create the diagnostic
                var primaryLocation = declaration.GetLocation();
                var diagnostic = Diagnostic.Create(
                    NewInMethodRule,
                    primaryLocation,
                    new string[] { declaration.Identifier.ValueText }
                    );
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes a PropertyDeclarationSyntax Node for use of the 'new' keyword
        /// </summary>
        /// <param name="context">The context of the PropertyDeclarationSyntax Node</param>
        private static void AnalyzePropertyNode(SyntaxNodeAnalysisContext context)
        {
            var declaration = context.Node as PropertyDeclarationSyntax;
            var modifiers = declaration.Modifiers;
            if (modifiers.Any(m => m.IsKind(SyntaxKind.NewKeyword)))
            {
                // Create the diagnostic
                var primaryLocation = declaration.GetLocation();
                var diagnostic = Diagnostic.Create(
                    NewInPropertyRule,
                    primaryLocation,
                    new string[] { declaration.Identifier.ValueText }
                    );
                context.ReportDiagnostic(diagnostic);
            }
        }
            
    }
}
