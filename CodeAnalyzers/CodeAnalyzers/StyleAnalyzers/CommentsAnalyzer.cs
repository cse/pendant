﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Collections.Immutable;
using System.Resources;
using System.Globalization;
using System.Threading;
using System.Xml.Linq;
using System.Linq;
using CodeAnalyzers;

namespace KSU.CS.Pendant.CodeAnalysis.StyleAnalyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class CommentsAnalyzer : DiagnosticAnalyzer
    {

        #region Metadata 

        private const string Category = "Documenting";

        // Class metadata
        private const string xmlc01DiagnosticId = "XMLC0001";
        private const string xmlc01HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0001";
        private static readonly LocalizableString xmlc01Title = new LocalizableResourceString(nameof(Resources.XMLC01AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc01MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC01AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc01Description = new LocalizableResourceString(nameof(Resources.XMLC01AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc01Rule = new DiagnosticDescriptor(xmlc01DiagnosticId, xmlc01Title, xmlc01MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc01Description, helpLinkUri: xmlc01HelpLinkUri);

        // Struct metadata
        private const string xmlc02DiagnosticId = "XMLC0002";
        private const string xmlc02HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0002";
        private static readonly LocalizableString xmlc02Title = new LocalizableResourceString(nameof(Resources.XMLC02AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc02MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC02AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc02Description = new LocalizableResourceString(nameof(Resources.XMLC02AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc02Rule = new DiagnosticDescriptor(xmlc02DiagnosticId, xmlc02Title, xmlc02MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc02Description, helpLinkUri: xmlc02HelpLinkUri);

        // Interface metadata
        private const string xmlc03DiagnosticId = "XMLC0003";
        private const string xmlc03HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0003";
        private static readonly LocalizableString xmlc03Title = new LocalizableResourceString(nameof(Resources.XMLC03AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc03MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC03AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc03Description = new LocalizableResourceString(nameof(Resources.XMLC03AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc03Rule = new DiagnosticDescriptor(xmlc03DiagnosticId, xmlc03Title, xmlc03MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc03Description, helpLinkUri: xmlc03HelpLinkUri);

        // Enum metadata
        private const string xmlc04DiagnosticId = "XMLC0004";
        private const string xmlc04HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0004";
        private static readonly LocalizableString xmlc04Title = new LocalizableResourceString(nameof(Resources.XMLC04AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc04MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC04AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc04Description = new LocalizableResourceString(nameof(Resources.XMLC04AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc04Rule = new DiagnosticDescriptor(xmlc04DiagnosticId, xmlc04Title, xmlc04MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc04Description, helpLinkUri: xmlc04HelpLinkUri);

        // Field metadata
        private const string xmlc05DiagnosticId = "XMLC0005";
        private const string xmlc05HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0005";
        private static readonly LocalizableString xmlc05Title = new LocalizableResourceString(nameof(Resources.XMLC05AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc05MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC05AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc05Description = new LocalizableResourceString(nameof(Resources.XMLC05AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc05Rule = new DiagnosticDescriptor(xmlc05DiagnosticId, xmlc05Title, xmlc05MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc05Description, helpLinkUri: xmlc05HelpLinkUri);

        // Property metadata
        private const string xmlc06DiagnosticId = "XMLC0006";
        private const string xmlc06HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0006";
        private static readonly LocalizableString xmlc06Title = new LocalizableResourceString(nameof(Resources.XMLC06AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc06MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC06AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc06Description = new LocalizableResourceString(nameof(Resources.XMLC06AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc06Rule = new DiagnosticDescriptor(xmlc06DiagnosticId, xmlc06Title, xmlc06MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc06Description, helpLinkUri: xmlc06HelpLinkUri);

        // Method metadata
        private const string xmlc07DiagnosticId = "XMLC0007";
        private const string xmlc07HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0007";
        private static readonly LocalizableString xmlc07Title = new LocalizableResourceString(nameof(Resources.XMLC07AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc07MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC07AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc07Description = new LocalizableResourceString(nameof(Resources.XMLC07AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc07Rule = new DiagnosticDescriptor(xmlc07DiagnosticId, xmlc07Title, xmlc07MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc07Description, helpLinkUri: xmlc07HelpLinkUri);

        // Method parameter metadata
        private const string xmlc08DiagnosticId = "XMLC0008";
        private const string xmlc08HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0008";
        private static readonly LocalizableString xmlc08Title = new LocalizableResourceString(nameof(Resources.XMLC08AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc08MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC08AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc08Description = new LocalizableResourceString(nameof(Resources.XMLC08AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc08Rule = new DiagnosticDescriptor(xmlc08DiagnosticId, xmlc08Title, xmlc08MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc08Description, helpLinkUri: xmlc08HelpLinkUri);

        // Method parameter metadata
        private const string xmlc09DiagnosticId = "XMLC0009";
        private const string xmlc09HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0009";
        private static readonly LocalizableString xmlc09Title = new LocalizableResourceString(nameof(Resources.XMLC09AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc09MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC09AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc09Description = new LocalizableResourceString(nameof(Resources.XMLC09AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc09Rule = new DiagnosticDescriptor(xmlc09DiagnosticId, xmlc09Title, xmlc09MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc09Description, helpLinkUri: xmlc09HelpLinkUri);

        // XML formatting metadata
        private const string xmlc10DiagnosticId = "XMLC0010";
        private const string xmlc10HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/XMLC0010";
        private static readonly LocalizableString xmlc10Title = new LocalizableResourceString(nameof(Resources.XMLC10AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc10MessageFormat = new LocalizableResourceString(nameof(Resources.XMLC10AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString xmlc10Description = new LocalizableResourceString(nameof(Resources.XMLC10AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor xmlc10Rule = new DiagnosticDescriptor(xmlc10DiagnosticId, xmlc10Title, xmlc10MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: xmlc10Description, helpLinkUri: xmlc10HelpLinkUri);

        #endregion

        #region Helper Methods

        /// <summary>
        /// A helper function to get the XML comments (if any) for a SyntaxNode
        /// </summary>
        /// <param name="node">The SyntaxNode to examine for comments</param>
        /// <returns>An XElement instance or null if no comments were found</returns>
        static XElement GetXmlComments(SyntaxNode node)
        {
            SyntaxTrivia trivia = node.GetLeadingTrivia()
                .FirstOrDefault(t => t.IsKind(SyntaxKind.MultiLineDocumentationCommentTrivia) || t.IsKind(SyntaxKind.SingleLineDocumentationCommentTrivia));
            var kind = trivia.Kind();
            if (trivia.Kind() == SyntaxKind.None) return null;
            var structure = $"<comment>{trivia.GetStructure().ToString().Replace("///", "")}</comment>";
            return XDocument.Parse(structure).Element("comment");
        }

        /// <summary>
        /// A helper method to check that a populated summary tag exists in the XML comment
        /// </summary>
        /// <param name="xmlComment">The XML comment to check</param>
        /// <returns>True if the summary is misisng or empty, true otherwise</returns>
        static bool CheckForMissingSummary(XElement xmlComment)
        {
            var summary = xmlComment.Element("summary");
            return summary is null || string.IsNullOrWhiteSpace(summary.Value);
        }

        /// <summary>
        /// A helper method to check for a populated param tag exists in the XML comment
        /// </summary>
        /// <param name="xmlComment">The XML Comment to check</param>
        /// <param name="paramName">The name of the parameter to look for</param>
        /// <returns>True if the param is missing or empty, false otherwise</returns>
        static bool CheckForMissingParam(XElement xmlComment, string paramName)
        {
            var param = xmlComment.Elements("param").FirstOrDefault(p => !(p.Attribute("name") is null) && p.Attribute("name").Value == paramName);
            return param is null || string.IsNullOrWhiteSpace(param.Value);
        }

        /// <summary>
        /// A helper method to check for a populated returns tag exists in the XML comment
        /// </summary>
        /// <param name="xmlComment">The XML Comment to check</param>
        /// <returns>True if the param is missing or empty, false otherwise</returns>
        static bool CheckForMissingReturns(XElement xmlComment)
        {
            var returns = xmlComment.Element("returns");
            return returns is null || string.IsNullOrWhiteSpace(returns.Value);
        }

        #endregion

        /// <summary>
        /// An immutable array of the diagnostics that returns a new ImmutableArray with the new rules added
        /// </summary>
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(xmlc01Rule, xmlc02Rule, xmlc03Rule, xmlc04Rule, xmlc05Rule, xmlc06Rule, xmlc07Rule, xmlc08Rule, xmlc09Rule, xmlc10Rule); ; } }

        /// <summary>
        /// Initializer for the analyzer
        /// </summary>
        /// <param name="context">The analysis that runs methods constantly when the page is open.</param>
        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(AnalyzeCommentOfClasses, SyntaxKind.ClassDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeCommentOfStructs, SyntaxKind.StructDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeCommentOfInterfaces, SyntaxKind.InterfaceDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeCommentOfEnums, SyntaxKind.EnumDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeCommentOfFields, SyntaxKind.FieldDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeCommentOfProperties, SyntaxKind.PropertyDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeCommentOfMethods, SyntaxKind.MethodDeclaration);
        }

        /// <summary>
        /// Analyzes classes in the document to make sure they have proper XML Comment Documentation.
        /// </summary>
        /// <param name="context"> The syntax node we are checking </param>
        private static void AnalyzeCommentOfClasses(SyntaxNodeAnalysisContext context)
        {
            var classDeclaration = (ClassDeclarationSyntax)context.Node;
            try
            {
                var comments = GetXmlComments(classDeclaration);

                if (comments is null || CheckForMissingSummary(comments))
                {
                    var diagnostic = Diagnostic.Create(xmlc01Rule, classDeclaration.Identifier.GetLocation(), classDeclaration.Identifier);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            catch (System.Xml.XmlException)
            {
                var diagnostic = Diagnostic.Create(xmlc10Rule, classDeclaration.Identifier.GetLocation(), "class", classDeclaration.Identifier);
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes structs in the document to make sure they have proper XML Comment Documentation.
        /// </summary>
        /// <param name="context"> The syntax node we are checking </param>
        private static void AnalyzeCommentOfStructs(SyntaxNodeAnalysisContext context)
        {
            var structDeclaration = (StructDeclarationSyntax)context.Node;
            try
            { 
                var comments = GetXmlComments(structDeclaration);

                if (comments is null || CheckForMissingSummary(comments))
                {
                    var diagnostic = Diagnostic.Create(xmlc02Rule, structDeclaration.Identifier.GetLocation(), structDeclaration.Identifier);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            catch (System.Xml.XmlException)
            {
                var diagnostic = Diagnostic.Create(xmlc10Rule, structDeclaration.Identifier.GetLocation(), "struct", structDeclaration.Identifier);
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes interfaces in the document to make sure they have proper XML Comment Documentation.
        /// </summary>
        /// <param name="context"> The syntax node we are checking </param>
        private static void AnalyzeCommentOfInterfaces(SyntaxNodeAnalysisContext context)
        {
            var interfaceDeclaration = (InterfaceDeclarationSyntax)context.Node;

            try
            {
                var comments = GetXmlComments(interfaceDeclaration);

                if (comments is null || CheckForMissingSummary(comments))
                {
                    var diagnostic = Diagnostic.Create(xmlc03Rule, interfaceDeclaration.Identifier.GetLocation(), interfaceDeclaration.Identifier);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            catch (System.Xml.XmlException)
            {
                var diagnostic = Diagnostic.Create(xmlc10Rule, interfaceDeclaration.Identifier.GetLocation(), "interface", interfaceDeclaration.Identifier);
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes enums in the document to make sure they have proper XML Comment Documentation.
        /// </summary>
        /// <param name="context"> The syntax node we are checking </param>
        private static void AnalyzeCommentOfEnums(SyntaxNodeAnalysisContext context)
        {
            var enumDeclaration = (EnumDeclarationSyntax)context.Node;

            try 
            { 
                var comments = GetXmlComments(enumDeclaration);

                if (comments is null || CheckForMissingSummary(comments))
                {
                    var diagnostic = Diagnostic.Create(xmlc04Rule, enumDeclaration.Identifier.GetLocation(), enumDeclaration.Identifier);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            catch (System.Xml.XmlException)
            {
                var diagnostic = Diagnostic.Create(xmlc10Rule, enumDeclaration.Identifier.GetLocation(), "enum", enumDeclaration.Identifier);
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes fields in the document to make sure they have proper XML Comment Documentation.
        /// </summary>
        /// <param name="context"> The syntax node we are checking </param>
        private static void AnalyzeCommentOfFields(SyntaxNodeAnalysisContext context)
        {
            //Finds the field delcarations within the cs file
            var fieldDeclaration = (FieldDeclarationSyntax)context.Node;

            try
            {
                var comments = GetXmlComments(fieldDeclaration);

                if (comments is null || CheckForMissingSummary(comments))
                {
                    var identifiers = string.Join(", ", fieldDeclaration.Declaration.Variables.Select(v => v.Identifier));
                    var diagnostic = Diagnostic.Create(xmlc05Rule, fieldDeclaration.GetLocation(), identifiers);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            catch (System.Xml.XmlException)
            {
                var identifiers = string.Join(", ", fieldDeclaration.Declaration.Variables.Select(v => v.Identifier));
                var diagnostic = Diagnostic.Create(xmlc10Rule, fieldDeclaration.GetLocation(), "field", identifiers);
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes properties in the document to make sure they have proper XML Comment Documentation.
        /// </summary>
        /// <param name="context"> The syntax node we are checking </param>
        private static void AnalyzeCommentOfProperties(SyntaxNodeAnalysisContext context)
        {
            var propertyDeclaration = (PropertyDeclarationSyntax)context.Node;

            try 
            { 
                var comments = GetXmlComments(propertyDeclaration);

                if (comments is null || CheckForMissingSummary(comments))
                {
                    var diagnostic = Diagnostic.Create(xmlc06Rule, propertyDeclaration.Identifier.GetLocation(), propertyDeclaration.Identifier);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            catch (System.Xml.XmlException)
            {
                var diagnostic = Diagnostic.Create(xmlc10Rule, propertyDeclaration.Identifier.GetLocation(), "property", propertyDeclaration.Identifier);
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes methods in the document to make sure they have proper XML Comment Documentation.
        /// </summary>
        /// <param name="context"> The syntax node we are checking </param>
        private static void AnalyzeCommentOfMethods(SyntaxNodeAnalysisContext context)
        {
            var methodDeclaration = (MethodDeclarationSyntax)context.Node;

            try { 
                var comments = GetXmlComments(methodDeclaration);

                // Check for summary tag
                if (comments is null || CheckForMissingSummary(comments))
                {
                    var diagnostic = Diagnostic.Create(xmlc07Rule, methodDeclaration.Identifier.GetLocation(), methodDeclaration.Identifier);
                    context.ReportDiagnostic(diagnostic);
                }

                // Check for param tags (if any are needed)
                foreach (var parameter in methodDeclaration.ParameterList.Parameters)
                {
                    if (comments is null || CheckForMissingParam(comments, parameter.Identifier.ToString()))
                    {
                        var diagnostic = Diagnostic.Create(xmlc08Rule, methodDeclaration.Identifier.GetLocation(), methodDeclaration.Identifier, parameter.Identifier);
                        context.ReportDiagnostic(diagnostic);
                    }
                }

                // Check for returns tag (if not void)
                if(methodDeclaration.ReturnType.ToString() != "void" && (comments is null || CheckForMissingReturns(comments)))
                {
                    var diagnostic = Diagnostic.Create(xmlc09Rule, methodDeclaration.Identifier.GetLocation(), methodDeclaration.Identifier);
                    context.ReportDiagnostic(diagnostic);
                }
            }
            catch (System.Xml.XmlException)
            {
                var diagnostic = Diagnostic.Create(xmlc10Rule, methodDeclaration.Identifier.GetLocation(), "method", methodDeclaration.Identifier);
                context.ReportDiagnostic(diagnostic);
            }
        }


    }
}
