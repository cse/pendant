﻿/* NamingConventionsAnalyzer.cs
* Authors: Nathan Bean, Austin Hess
*/
using System;
using System.Collections.Immutable;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using CodeAnalyzers;

namespace KSU.CS.Pendant.CodeAnalysis.StyleAnalyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class NamingConventionsAnalyzer : DiagnosticAnalyzer
    {
        #region Metadata 

        private const string Category = "Naming";

        // Namespace metadata
        private const string Nam01DiagnosticId = "NAM0001";
        private const string Nam01HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0001";
        private static readonly LocalizableString Nam01Title = new LocalizableResourceString(nameof(Resources.NAM01AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam01MessageFormat = new LocalizableResourceString(nameof(Resources.NAM01AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam01Description = new LocalizableResourceString(nameof(Resources.NAM01AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam01Rule = new DiagnosticDescriptor(Nam01DiagnosticId, Nam01Title, Nam01MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam01Description, helpLinkUri: Nam01HelpLinkUri);

        // Class names should be in Pascal Case metatdata
        private const string Nam02DiagnosticId = "NAM0002";
        private const string Nam02HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0002";
        private static readonly LocalizableString Nam02Title = new LocalizableResourceString(nameof(Resources.NAM02AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam02MessageFormat = new LocalizableResourceString(nameof(Resources.NAM02AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam02Description = new LocalizableResourceString(nameof(Resources.NAM02AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam02Rule = new DiagnosticDescriptor(Nam02DiagnosticId, Nam02Title, Nam02MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam02Description, helpLinkUri: Nam02HelpLinkUri);

        // Class names should not begin with I followed by a capital letter metadata 
        private const string Nam03DiagnosticId = "NAM0003";
        private const string Nam03HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0003";
        private static readonly LocalizableString Nam03Title = new LocalizableResourceString(nameof(Resources.NAM03AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam03MessageFormat = new LocalizableResourceString(nameof(Resources.NAM03AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam03Description = new LocalizableResourceString(nameof(Resources.NAM03AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam03Rule = new DiagnosticDescriptor(Nam03DiagnosticId, Nam03Title, Nam03MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam03Description, helpLinkUri: Nam03HelpLinkUri);

        // Struct names should be in Pascal Case metatdata
        private const string Nam04DiagnosticId = "NAM0004";
        private const string Nam04HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0004";
        private static readonly LocalizableString Nam04Title = new LocalizableResourceString(nameof(Resources.NAM04AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam04MessageFormat = new LocalizableResourceString(nameof(Resources.NAM04AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam04Description = new LocalizableResourceString(nameof(Resources.NAM04AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam04Rule = new DiagnosticDescriptor(Nam04DiagnosticId, Nam04Title, Nam04MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam04Description, helpLinkUri: Nam04HelpLinkUri);

        // Struct names should not begin with I followed by a capital letter metadata 
        private const string Nam05DiagnosticId = "NAM0005";
        private const string Nam05HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0005";
        private static readonly LocalizableString Nam05Title = new LocalizableResourceString(nameof(Resources.NAM05AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam05MessageFormat = new LocalizableResourceString(nameof(Resources.NAM05AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam05Description = new LocalizableResourceString(nameof(Resources.NAM05AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam05Rule = new DiagnosticDescriptor(Nam05DiagnosticId, Nam05Title, Nam05MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam05Description, helpLinkUri: Nam05HelpLinkUri);

        // Struct names should be in Pascal Case metatdata
        private const string Nam06DiagnosticId = "NAM0006";
        private const string Nam06HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0006";
        private static readonly LocalizableString Nam06Title = new LocalizableResourceString(nameof(Resources.NAM06AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam06MessageFormat = new LocalizableResourceString(nameof(Resources.NAM06AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam06Description = new LocalizableResourceString(nameof(Resources.NAM06AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam06Rule = new DiagnosticDescriptor(Nam06DiagnosticId, Nam06Title, Nam06MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam06Description, helpLinkUri: Nam06HelpLinkUri);

        // Struct names should not begin with I followed by a capital letter metadata 
        private const string Nam07DiagnosticId = "NAM0007";
        private const string Nam07HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0007";
        private static readonly LocalizableString Nam07Title = new LocalizableResourceString(nameof(Resources.NAM07AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam07MessageFormat = new LocalizableResourceString(nameof(Resources.NAM07AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam07Description = new LocalizableResourceString(nameof(Resources.NAM07AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam07Rule = new DiagnosticDescriptor(Nam07DiagnosticId, Nam07Title, Nam07MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam07Description, helpLinkUri: Nam07HelpLinkUri);

        // Interface names should begin with an I metadata
        private const string Nam08DiagnosticId = "NAM0008";
        private const string Nam08HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0008";
        private static readonly LocalizableString Nam08Title = new LocalizableResourceString(nameof(Resources.NAM08AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam08MessageFormat = new LocalizableResourceString(nameof(Resources.NAM08AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam08Description = new LocalizableResourceString(nameof(Resources.NAM08AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam08Rule = new DiagnosticDescriptor(Nam08DiagnosticId, Nam08Title, Nam08MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam08Description, helpLinkUri: Nam08HelpLinkUri);

        // Interface names should be in Pascal Case metadata
        private const string Nam09DiagnosticId = "NAM0009";
        private const string Nam09HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0009";
        private static readonly LocalizableString Nam09Title = new LocalizableResourceString(nameof(Resources.NAM09AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam09MessageFormat = new LocalizableResourceString(nameof(Resources.NAM09AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam09Description = new LocalizableResourceString(nameof(Resources.NAM09AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam09Rule = new DiagnosticDescriptor(Nam09DiagnosticId, Nam09Title, Nam09MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam09Description, helpLinkUri: Nam09HelpLinkUri);

        // Method names should be in Pascal Case metadata
        private const string Nam10DiagnosticId = "NAM0010";
        private const string Nam10HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0010";
        private static readonly LocalizableString Nam10Title = new LocalizableResourceString(nameof(Resources.NAM10AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam10MessageFormat = new LocalizableResourceString(nameof(Resources.NAM10AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam10Description = new LocalizableResourceString(nameof(Resources.NAM10AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam10Rule = new DiagnosticDescriptor(Nam10DiagnosticId, Nam10Title, Nam10MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam10Description, helpLinkUri: Nam10HelpLinkUri);

        // Property names should be in Pascal Case metadata
        private const string Nam11DiagnosticId = "NAM0011";
        private const string Nam11HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0011";
        private static readonly LocalizableString Nam11Title = new LocalizableResourceString(nameof(Resources.NAM11AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam11MessageFormat = new LocalizableResourceString(nameof(Resources.NAM11AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam11Description = new LocalizableResourceString(nameof(Resources.NAM11AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam11Rule = new DiagnosticDescriptor(Nam11DiagnosticId, Nam11Title, Nam11MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam11Description, helpLinkUri: Nam11HelpLinkUri);

        // Parameter names should be in Camel Case metadata
        private const string Nam12DiagnosticId = "NAM0012";
        private const string Nam12HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0012";
        private static readonly LocalizableString Nam12Title = new LocalizableResourceString(nameof(Resources.NAM12AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam12MessageFormat = new LocalizableResourceString(nameof(Resources.NAM12AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam12Description = new LocalizableResourceString(nameof(Resources.NAM12AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam12Rule = new DiagnosticDescriptor(Nam12DiagnosticId, Nam12Title, Nam12MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam12Description, helpLinkUri: Nam12HelpLinkUri);

        // Constant fields should be in Camel Case metadata
        private const string Nam13DiagnosticId = "NAM0013";
        private const string Nam13HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0013";
        private static readonly LocalizableString Nam13Title = new LocalizableResourceString(nameof(Resources.NAM13AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam13MessageFormat = new LocalizableResourceString(nameof(Resources.NAM13AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam13Description = new LocalizableResourceString(nameof(Resources.NAM13AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam13Rule = new DiagnosticDescriptor(Nam13DiagnosticId, Nam13Title, Nam13MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam13Description, helpLinkUri: Nam13HelpLinkUri);

        // private and protected fields should be prefixed with underscore metadata
        private const string Nam14DiagnosticId = "NAM0014";
        private const string Nam14HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0014";
        private static readonly LocalizableString Nam14Title = new LocalizableResourceString(nameof(Resources.NAM14AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam14MessageFormat = new LocalizableResourceString(nameof(Resources.NAM14AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam14Description = new LocalizableResourceString(nameof(Resources.NAM14AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam14Rule = new DiagnosticDescriptor(Nam14DiagnosticId, Nam14Title, Nam14MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam14Description, helpLinkUri: Nam14HelpLinkUri);

        // private and protected fields should be in camel case metadata
        private const string Nam15DiagnosticId = "NAM0015";
        private const string Nam15HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0015";
        private static readonly LocalizableString Nam15Title = new LocalizableResourceString(nameof(Resources.NAM15AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam15MessageFormat = new LocalizableResourceString(nameof(Resources.NAM15AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam15Description = new LocalizableResourceString(nameof(Resources.NAM15AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam15Rule = new DiagnosticDescriptor(Nam15DiagnosticId, Nam15Title, Nam15MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam15Description, helpLinkUri: Nam15HelpLinkUri);

        // local variables should be in camel case metadata
        private const string Nam16DiagnosticId = "NAM0016";
        private const string Nam16HelpLinkUri = "https://pendant.cs.ksu.edu/Diagnostic/NAM0016";
        private static readonly LocalizableString Nam16Title = new LocalizableResourceString(nameof(Resources.NAM16AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam16MessageFormat = new LocalizableResourceString(nameof(Resources.NAM16AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Nam16Description = new LocalizableResourceString(nameof(Resources.NAM16AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private static readonly DiagnosticDescriptor Nam16Rule = new DiagnosticDescriptor(Nam16DiagnosticId, Nam16Title, Nam16MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Nam16Description, helpLinkUri: Nam16HelpLinkUri);


        /// <summary>
        /// An immutable array of the diagnostics that returns a new ImmutableArray with the new rule added
        /// </summary>
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(Nam01Rule, Nam02Rule, Nam03Rule, Nam04Rule, Nam05Rule, Nam06Rule, Nam07Rule, Nam08Rule, Nam09Rule, Nam10Rule, Nam11Rule, Nam12Rule, Nam13Rule, Nam14Rule, Nam15Rule, Nam16Rule); } }

        #endregion

        #region Helper Methods 

        /// <summary>
        /// Checks that the string <paramref name="s"/> is in camel case
        /// </summary>
        /// <param name="s">The string to check</param>
        /// <returns>true if <paramref name="s"/> is in camel case, false if not</returns>
        private static bool IsCamelCase(string s)
        {
            return Char.IsLetter(s[0]) && Char.IsLower(s[0]);
        }

        /// <summary>
        /// Checks that the string <paramref name="s"/> is in Pascal case
        /// </summary>
        /// <param name="s">The string to check</param>
        /// <returns>true if <paramref name="s"/> is in Pascal case, false if not</returns>
        private static bool IsPascalCase(string s)
        {
            return Char.IsLetter(s[0]) && Char.IsUpper(s[0]);
        }

        /// <summary>
        /// Checks that string <paramref name="s"/> follows interface declaration style
        /// A capital "I" followed by a second capital letter.
        /// </summary>
        /// <param name="s">The string to check</param>
        /// <returns>true if <paramref name="s"/> follows the interface rule, false if not</returns>
        private static bool IsNamedLikeInterface(string s)
        {
            return s[0] == 'I' && Char.IsUpper(s[1]);
        }

        #endregion

        /// <summary>
        /// Initializer for the analyzer
        /// </summary>
        /// <param name="context">The analysis that runs methods constantly when the page is open.</param>
        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfNamespaces, SyntaxKind.NamespaceDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfClasses, SyntaxKind.ClassDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfStructs, SyntaxKind.StructDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfEnums, SyntaxKind.EnumDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfInterfaces, SyntaxKind.InterfaceDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfMethods, SyntaxKind.MethodDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfProperties, SyntaxKind.PropertyDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfParameters, SyntaxKind.Parameter);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfFields, SyntaxKind.FieldDeclaration);
            context.RegisterSyntaxNodeAction(AnalyzeNamingConventionsOfLocalVariables, SyntaxKind.LocalDeclarationStatement);
        }

        /// <summary>
        /// Analyzes Namespace declarations for compliance with naming conventions
        /// </summary>
        /// <param name="context">The NamespaceDeclarationSyntax node to check</param>
        private static void AnalyzeNamingConventionsOfNamespaces(SyntaxNodeAnalysisContext context)
        {
            var namespaceDeclaration = (NamespaceDeclarationSyntax)context.Node;
            var name = namespaceDeclaration.ChildNodes().OfType<QualifiedNameSyntax>().FirstOrDefault();
            if (name is null) return;
            var segments = name.ToString().Split('.');
            foreach (var segment in segments)
            {
                if (!IsPascalCase(segment))
                {
                    //Creates the diagnostic
                    var diagnostic = Diagnostic.Create(
                        Nam01Rule,
                        name.GetLocation(),
                        name, segment
                        );
                    context.ReportDiagnostic(diagnostic);
                }
            }
        }

        /// <summary>
        /// Analyzes the names of Classes to see if they comply with the naming conventions of CIS 300 and CIS 400 at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfClasses(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a ClassDeclarationSyntax
            var declaration = (ClassDeclarationSyntax)context.Node;

            // Grab the identifier
            var id = declaration.Identifier.ValueText;

            // Classes should be declared with Pascal case 
            if (!IsPascalCase(id))
            {
                // Create a diagnostic at the location of the class name
                var diagnostic = Diagnostic.Create(
                    Nam02Rule,
                    declaration.Identifier.GetLocation(),
                    id
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }

            // Classes should not begin with an "I" followed by a capital letter
            if (IsNamedLikeInterface(id))
            {
                // Create a diagnostic at the location of the class name
                var diagnostic = Diagnostic.Create(
                    Nam03Rule,
                    declaration.Identifier.GetLocation(),
                    id, id[1]
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes the names of Structs to see if they comply with naming conventions of CIS 300 and CIS 400 at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfStructs(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a StructDeclarationSyntax
            var declaration = (StructDeclarationSyntax)context.Node;

            // Grab the identifier
            var id = declaration.Identifier.ValueText;

            // Structs should be declared with Pascal case 
            if (!IsPascalCase(id))
            {
                // Create a diagnostic at the location of the struct name
                var diagnostic = Diagnostic.Create(
                    Nam04Rule,
                    declaration.Identifier.GetLocation(),
                    id
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }

            // Structs should not begin with an "I" followed by a capital letter
            if (IsNamedLikeInterface(id))
            {
                // Create a diagnostic at the location of the struct name
                var diagnostic = Diagnostic.Create(
                    Nam05Rule,
                    declaration.Identifier.GetLocation(),
                    id, id[1]
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes the names of Enums to see if they comply with the naming conventions of CIS 300 and CIS 400 at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfEnums(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is an EnumDeclarationSyntax
            var declaration = (EnumDeclarationSyntax)context.Node;

            // Grab the identifier
            var id = declaration.Identifier.ValueText;

            // Enums should be declared with Pascal case 
            if (!IsPascalCase(id))
            {
                // Create a diagnostic at the location of the struct name
                var diagnostic = Diagnostic.Create(
                    Nam06Rule,
                    declaration.Identifier.GetLocation(),
                    id
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }

            // Enums should not begin with an "I" followed by a capital letter
            if (IsNamedLikeInterface(id))
            {
                // Create a diagnostic at the location of the struct name
                var diagnostic = Diagnostic.Create(
                    Nam07Rule,
                    declaration.Identifier.GetLocation(),
                    id, id[1]
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes the naming of interfaces to see if they comply with CIS 300 and CIS 400 naming conventions at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfInterfaces(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a InterfaceDeclarationSyntax
            var declaration = (InterfaceDeclarationSyntax)context.Node;

            // Grab the identifier
            var id = declaration.Identifier.ValueText;

            // Interface names should be prefixed with an "I"
            if (id[0] != 'I')
            {
                // Create a diagnostic at the location of the interface name
                var diagnostic = Diagnostic.Create(
                    Nam08Rule,
                    declaration.Identifier.GetLocation(),
                    id
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
                // Avoid testing for Pascal Case until prefix is fixed
                return;
            }

            // After the prefix, interface names should be in Pascal case 
            if (!IsPascalCase(id.Substring(1)))
            {
                // Create a diagnostic at the location of the class name
                var diagnostic = Diagnostic.Create(
                    Nam09Rule,
                    declaration.Identifier.GetLocation(),
                    id, id[0]
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes the naming of interfacees to see if they comply with CIS 300 and CIS 400 naming conventions at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfMethods(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a MethodDeclarationSyntax
            var declaration = (MethodDeclarationSyntax)context.Node;

            // Grab the identifier
            var id = declaration.Identifier.ValueText;

            // Method names should be in Pascal case 
            if (!IsPascalCase(id))
            {
                // Create a diagnostic at the location of the class name
                var diagnostic = Diagnostic.Create(
                    Nam10Rule,
                    declaration.Identifier.GetLocation(),
                    id
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes the naming of properties to see if they comply with CIS 300 and CIS 400 naming conventions at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfProperties(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a PropertyDeclarationSyntax
            var declaration = (PropertyDeclarationSyntax)context.Node;

            // Grab the identifier
            var id = declaration.Identifier.ValueText;

            // Method names should be in Pascal case 
            if (!IsPascalCase(id))
            {
                // Create a diagnostic at the location of the class name
                var diagnostic = Diagnostic.Create(
                    Nam11Rule,
                    declaration.Identifier.GetLocation(),
                    id
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes the naming of parameters to see if they comply with CIS 300 and CIS 400 naming conventions at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfParameters(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a ParameterSyntax
            var declaration = (ParameterSyntax)context.Node;

            // get the variable identifier
            var id = declaration.Identifier.ValueText;

            // parameter names should be in camel case 
            if (!IsCamelCase(id))
            {
                // Create a diagnostic at the location of the variable name
                var diagnostic = Diagnostic.Create(
                    Nam12Rule,
                    declaration.Identifier.GetLocation(),
                    id
                    );
                // Reports the problem in the code
                context.ReportDiagnostic(diagnostic);
            }
        }

        /// <summary>
        /// Analyzes the naming of local variables to see if they comply with CIS 300 and CIS 400 naming conventions at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfFields(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a FieldDeclarationSyntax
            var declaration = (FieldDeclarationSyntax)context.Node;
            var modifiers = declaration.Modifiers;

            // public const field names should be in Pascal case 
            if (modifiers.Any(SyntaxKind.ConstKeyword) && modifiers.Any(SyntaxKind.PublicKeyword))
            {
                // We may have multiple variables 
                foreach (var dec in declaration.Declaration.Variables)
                {
                    // get the variable identifier
                    var id = dec.Identifier.ValueText;

                    // public const field names should be in camel case 
                    if (!IsPascalCase(id))
                    {
                        // Create a diagnostic at the location of the variable name
                        var diagnostic = Diagnostic.Create(
                            Nam13Rule,
                            dec.Identifier.GetLocation(),
                            id
                            );
                        // Reports the problem in the code
                        context.ReportDiagnostic(diagnostic);
                    }
                }
            }

            // private and protected field names should be in camel case and prefixed with an underscore
            if (modifiers.Any(SyntaxKind.PrivateKeyword) || modifiers.Any(SyntaxKind.ProtectedKeyword))
            {
                // We may have multiple variables 
                foreach (var dec in declaration.Declaration.Variables)
                {
                    // get the variable identifier
                    var id = dec.Identifier.ValueText;

                    // private and proteced field names should be prefixed with an underscore
                    if (id[0] != '_')
                    {
                        // Create a diagnostic at the location of the variable name
                        var diagnostic = Diagnostic.Create(
                            Nam14Rule,
                            dec.Identifier.GetLocation(),
                            id
                            );
                        // Reports the problem in the code
                        context.ReportDiagnostic(diagnostic);
                        // Skip checking camel case until underscore is fixed 
                        continue;
                    }

                    if (!IsCamelCase(id.Substring(1)))
                    {
                        // Create a diagnostic at the location of the variable name
                        var diagnostic = Diagnostic.Create(
                            Nam15Rule,
                            dec.Identifier.GetLocation(),
                            id
                            );
                        // Reports the problem in the code
                        context.ReportDiagnostic(diagnostic);
                    }
                }
            }
        }

        /// <summary>
        /// Analyzes the naming of local variables to see if they comply with CIS 300 and CIS 400 naming conventions at KSU
        /// </summary>
        /// <param name="context">The context of the class</param>
        private static void AnalyzeNamingConventionsOfLocalVariables(SyntaxNodeAnalysisContext context)
        {
            // The top node in the context is a FieldDeclarationSyntax
            var declaration = (LocalDeclarationStatementSyntax)context.Node;

            // We may have multiple variables            
            foreach (var dec in declaration.Declaration.Variables)
            {
                // get the variable identifier
                var id = dec.Identifier.ValueText;

                // local variable names should be in camel case 
                if (!IsCamelCase(id))
                {
                    // Create a diagnostic at the location of the variable name
                    var diagnostic = Diagnostic.Create(
                        Nam16Rule,
                        dec.Identifier.GetLocation(),
                        id
                        );
                    // Reports the problem in the code
                    context.ReportDiagnostic(diagnostic);
                }
            }
        }
    }
}
